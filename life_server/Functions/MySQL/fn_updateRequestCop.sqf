/*
	File: fn_updateRequest.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Ain't got time to describe it, READ THE FILE NAME!
*/
private["_uid","_side","_cash","_bank","_licenses","_gear","_name","_query","_thread","_thread","_position","_alive","_in_rea","_manger","_boire","_damage","_epargne"];
_uid = [_this,0,"",[""]] call BIS_fnc_param;
_name = [_this,1,"",[""]] call BIS_fnc_param;
_side = [_this,2,sideUnknown,[civilian]] call BIS_fnc_param;
_cash = [_this,3,0,[0]] call BIS_fnc_param;
_bank = [_this,4,5000,[0]] call BIS_fnc_param;
_licenses = [_this,5,[],[[]]] call BIS_fnc_param;
_gear = [_this,6,[],[[]]] call BIS_fnc_param;
//SavePos
_alive = [_this,10,false] call BIS_fnc_param;
_position = [_this,9,[]] call BIS_fnc_param;
//Saverea
_in_rea = [_this,11,false] call BIS_fnc_param;
_epargne = [_this,15,5000,[0]] call BIS_fnc_param;
//MANGER
//_manger = [_this,12,0,[]] call BIS_fnc_param;
//_boire = [_this,13,0,[]] call BIS_fnc_param;
//_damage = [_this,14,0,[]] call BIS_fnc_param;
//Get to those error checks.
if((_uid == "") OR (_name == "")) exitWith {};

//Parse and setup some data.
_name = [_name] call DB_fnc_mresString;
_gear = [_gear] call DB_fnc_mresArray;
_cash = [_cash] call DB_fnc_numberSafe;
_bank = [_bank] call DB_fnc_numberSafe;
//_manger = [_manger] call DB_fnc_numberSafe;
//_boire = [_boire] call DB_fnc_numberSafe;
//_damage = [_damage] call DB_fnc_numberSafe;

//Does something license related but I can't remember I only know it's important?
for "_i" from 0 to count(_licenses)-1 do {
	_bool = [(_licenses select _i) select 1] call DB_fnc_bool;
	_licenses set[_i,[(_licenses select _i) select 0,_bool]];
};

_licenses = [_licenses] call DB_fnc_mresArray;

switch (_side) do {
	case west: {_query = format["UPDATE players SET name='%1', cash='%2', bankacc='%3', cop_gear='%4', cop_licenses='%5', alive='%7', worldpos=""'%8'"", in_rea='%9', manger='%10', boire='%11', vie='%12', epargne='%13' WHERE playerid='%6'",_name,_cash,_bank,_gear,_licenses,_uid,[_alive] call DB_fnc_bool,_position,[_in_rea] call DB_fnc_bool,_this select 12,_this select 13,_this select 14,_epargne];};
};

waitUntil {sleep (random 0.3); !DB_Async_Active};
_queryResult = [_query,1] call DB_fnc_asyncCall;