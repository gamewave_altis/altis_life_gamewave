/*
Navicat MySQL Data Transfer

Source Server         : Arma3
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : arma3life

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2014-10-31 01:32:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `economy`
-- ----------------------------
DROP TABLE IF EXISTS `economy`;
CREATE TABLE `economy` (
  `numero` int(12) NOT NULL AUTO_INCREMENT,
  `ressource` varchar(32) NOT NULL,
  `sellprice` int(100) NOT NULL DEFAULT '0',
  `buyprice` int(100) NOT NULL DEFAULT '0',
  `varprice` int(100) NOT NULL,
  `minprice` int(100) NOT NULL,
  `maxprice` int(100) NOT NULL,
  `factor` enum('0','1','2','3','4','5','6','7') NOT NULL DEFAULT '0',
  `shoptype` text NOT NULL,
  `total_vendu` int(100) NOT NULL,
  PRIMARY KEY (`numero`),
  UNIQUE KEY `ressource` (`ressource`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of economy
-- ----------------------------
INSERT INTO `economy` VALUES ('1', 'heroinp', '3601', '0', '1', '1000', '50000', '2', 'heroin', '0');
INSERT INTO `economy` VALUES ('2', 'marijuana', '3401', '0', '1', '1000', '50000', '2', 'heroin', '0');
INSERT INTO `economy` VALUES ('3', 'meth', '8001', '0', '1', '1000', '50000', '2', 'heroin', '0');
INSERT INTO `economy` VALUES ('4', 'cocainep', '3901', '0', '1', '1000', '50000', '2', 'heroin', '0');
INSERT INTO `economy` VALUES ('5', 'oilp', '1502', '1502', '1', '500', '10000', '3', 'oil', '0');
INSERT INTO `economy` VALUES ('6', 'glass', '1485', '1500', '1', '500', '10000', '3', 'glass', '8');
INSERT INTO `economy` VALUES ('7', 'iron_r', '1495', '1500', '1', '500', '10000', '3', 'iron', '2');
INSERT INTO `economy` VALUES ('8', 'diamondc', '2202', '2202', '1', '500', '10000', '3', 'diamond', '0');
INSERT INTO `economy` VALUES ('9', 'salt_r', '2202', '2202', '1', '500', '10000', '3', 'salt', '0');
INSERT INTO `economy` VALUES ('10', 'copper_r', '2202', '2202', '1', '500', '10000', '3', 'iron', '0');
INSERT INTO `economy` VALUES ('11', 'cement', '1502', '1502', '1', '500', '10000', '3', 'cement', '0');
INSERT INTO `economy` VALUES ('12', 'goldbar', '10000', '10000', '1', '1', '50000', '1', 'gold', '0');
INSERT INTO `economy` VALUES ('13', 'turtle', '5846', '0', '1', '1000', '50000', '2', 'wongs', '1');

-- ----------------------------
-- Table structure for `gangs`
-- ----------------------------
DROP TABLE IF EXISTS `gangs`;
CREATE TABLE `gangs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner` varchar(32) DEFAULT NULL,
  `name` varchar(32) DEFAULT NULL,
  `members` text,
  `maxmembers` int(2) DEFAULT '8',
  `bank` int(100) DEFAULT '0',
  `active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of gangs
-- ----------------------------

-- ----------------------------
-- Table structure for `houses`
-- ----------------------------
DROP TABLE IF EXISTS `houses`;
CREATE TABLE `houses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` varchar(32) NOT NULL,
  `pos` varchar(64) DEFAULT '[]',
  `inventory` text,
  `containers` text,
  `owned` tinyint(4) DEFAULT '0',
  `timer` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`,`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of houses
-- ----------------------------

-- ----------------------------
-- Table structure for `logdeath`
-- ----------------------------
DROP TABLE IF EXISTS `logdeath`;
CREATE TABLE `logdeath` (
  `uid` int(12) NOT NULL AUTO_INCREMENT,
  `Killer` text NOT NULL,
  `Killed` text NOT NULL,
  `heure` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `uidKiller` text NOT NULL,
  `uidKilled` text NOT NULL,
  `Type` text NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of logdeath
-- ----------------------------

-- ----------------------------
-- Table structure for `messages`
-- ----------------------------
DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages` (
  `uid` int(12) NOT NULL AUTO_INCREMENT,
  `fromID` varchar(50) NOT NULL,
  `toID` varchar(50) NOT NULL,
  `message` text,
  `fromName` varchar(32) NOT NULL,
  `toName` varchar(32) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of messages
-- ----------------------------

-- ----------------------------
-- Table structure for `players`
-- ----------------------------
DROP TABLE IF EXISTS `players`;
CREATE TABLE `players` (
  `uid` int(12) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `playerid` varchar(50) NOT NULL,
  `cash` int(100) DEFAULT '0',
  `bankacc` int(100) DEFAULT '0',
  `coplevel` enum('0','1','2','3','4','5','6','7') NOT NULL DEFAULT '0',
  `cop_licenses` text,
  `civ_licenses` text,
  `cop_gear` text,
  `arrested` tinyint(1) NOT NULL DEFAULT '0',
  `aliases` text NOT NULL,
  `adminlevel` enum('0','1','2','3') NOT NULL DEFAULT '0',
  `donatorlvl` enum('0','1','2','3','4','5') NOT NULL DEFAULT '0',
  `civ_gear` text,
  `blacklist` tinyint(1) NOT NULL DEFAULT '0',
  `timestamp` int(11) DEFAULT NULL,
  `worldpos` text,
  `alive` tinyint(1) NOT NULL,
  `duredon` int(1) NOT NULL DEFAULT '0',
  `in_rea` tinyint(1) NOT NULL DEFAULT '0',
  `manger` int(4) NOT NULL DEFAULT '100',
  `boire` int(4) NOT NULL DEFAULT '100',
  `vie` int(4) NOT NULL DEFAULT '100',
  `epargne` int(100) DEFAULT '0',
  `restrained` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  UNIQUE KEY `playerid` (`playerid`),
  KEY `name` (`name`),
  KEY `blacklist` (`blacklist`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of players
-- ----------------------------
INSERT INTO `players` VALUES ('2', 'Prospere Lucel', '76561197960498085', '0', '147254336', '7', '\"[[`license_cop_air`,0],[`license_cop_swat`,0],[`license_cop_cg`,0]]\"', '\"[[`license_civ_driver`,1],[`license_civ_air`,0],[`license_civ_heroin`,0],[`license_civ_marijuana`,0],[`license_civ_gang`,0],[`license_civ_boat`,0],[`license_civ_oil`,0],[`license_civ_houblon`,0],[`license_civ_meth`,0],[`license_civ_dive`,0],[`license_civ_truck`,1],[`license_civ_piz`,0],[`license_civ_gla`,0],[`license_civ_gun`,0],[`license_civ_rebel`,1],[`license_civ_coke`,0],[`license_civ_diamond`,0],[`license_civ_copper`,0],[`license_civ_iron`,0],[`license_civ_sand`,0],[`license_civ_salt`,0],[`license_civ_cement`,0],[`license_civ_dep`,0],[`license_civ_home`,0],[`license_civ_taxi`,0],[`license_civ_medic`,0],[`license_civ_merc`,0]]\"', '\"[`U_Rangemaster`,`V_TacVest_blk_POLICE`,``,``,`H_Cap_police`,[`ItemMap`,`ItemCompass`,`ItemWatch`,`ItemGPS`],``,`hgun_P07_snds_F`,[],[],[],[],[],[`16Rnd_9x21_Mag`,`16Rnd_9x21_Mag`,`16Rnd_9x21_Mag`,`16Rnd_9x21_Mag`,`16Rnd_9x21_Mag`,`16Rnd_9x21_Mag`],[],[`muzzle_snds_L`,`Laserdesignator`,``],[`life_inv_apple`,`life_inv_apple`,`life_inv_apple`,`life_inv_rabbit`,`life_inv_donuts`,`life_inv_donuts`,`life_inv_donuts`,`life_inv_coffee`,`life_inv_coffee`,`life_inv_coffee`,`life_inv_cokep`,`life_inv_cokep`]]\"', '0', '\"[`Prospere Lucel`]\"', '3', '5', '\"[`U_C_Poloshirt_tricolour`,``,``,``,``,[`ItemMap`,`ItemCompass`,`ItemWatch`],``,``,[],[],[],[],[],[],[],[],[]]\"', '0', null, '\'[10280.3,10127.6,0.00152111]\'', '1', '0', '0', '100', '100', '100', '0', '0');

-- ----------------------------
-- Table structure for `vehicles`
-- ----------------------------
DROP TABLE IF EXISTS `vehicles`;
CREATE TABLE `vehicles` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `side` varchar(15) NOT NULL,
  `classname` varchar(32) NOT NULL,
  `type` varchar(12) NOT NULL,
  `pid` varchar(32) NOT NULL,
  `alive` tinyint(1) NOT NULL DEFAULT '1',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `plate` int(20) NOT NULL,
  `color` int(20) NOT NULL,
  `inventory` varchar(500) NOT NULL,
  `assur` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `side` (`side`),
  KEY `pid` (`pid`),
  KEY `type` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of vehicles
-- ----------------------------

-- ----------------------------
-- Procedure structure for `deleteDeadVehicles`
-- ----------------------------
DROP PROCEDURE IF EXISTS `deleteDeadVehicles`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteDeadVehicles`()
BEGIN
	DELETE FROM `vehicles` WHERE `alive` = 0 AND `assur` = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `deleteHouses`
-- ----------------------------
DROP PROCEDURE IF EXISTS `deleteHouses`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteHouses`()
BEGIN
  DELETE FROM houses WHERE timer < DATE_SUB(CURDATE(), INTERVAL 1 MONTH);
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `deleteOldGangs`
-- ----------------------------
DROP PROCEDURE IF EXISTS `deleteOldGangs`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteOldGangs`()
BEGIN
    DELETE FROM `gangs` WHERE `active` = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `deleteOldHouses`
-- ----------------------------
DROP PROCEDURE IF EXISTS `deleteOldHouses`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteOldHouses`()
BEGIN
  DELETE FROM `houses` WHERE `owned` = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `Function lucel`
-- ----------------------------
DROP PROCEDURE IF EXISTS `Function lucel`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `Function lucel`()
BEGIN
UPDATE players SET `bankacc`= 99999999;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `resetAssurVehicles`
-- ----------------------------
DROP PROCEDURE IF EXISTS `resetAssurVehicles`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `resetAssurVehicles`()
BEGIN
	UPDATE vehicles SET `alive`= 1 WHERE `assur`= 1;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `resetCopCash`
-- ----------------------------
DROP PROCEDURE IF EXISTS `resetCopCash`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `resetCopCash`()
BEGIN
	UPDATE players SET `bankacc`= 1250000 WHERE `bankacc`> 1250000 AND `coplevel`= 2;
	UPDATE players SET `bankacc`= 2850000 WHERE `bankacc`> 2850000 AND `coplevel`= 3;
	UPDATE players SET `bankacc`= 3500000 WHERE `bankacc`> 3500000 AND `coplevel`= 4;
	UPDATE players SET `bankacc`= 4700000 WHERE `bankacc`> 4700000 AND `coplevel`= 5;
	UPDATE players SET `bankacc`= 5900000 WHERE `bankacc`> 5900000 AND `coplevel`= 6;
	UPDATE players SET `bankacc`= 8500000 WHERE `bankacc`> 8500000 AND `coplevel`= 7;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `resetLifePosition`
-- ----------------------------
DROP PROCEDURE IF EXISTS `resetLifePosition`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `resetLifePosition`()
BEGIN
	UPDATE players SET `alive`= 0;
	UPDATE players SET `in_rea`= 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `resetLifeVehicles`
-- ----------------------------
DROP PROCEDURE IF EXISTS `resetLifeVehicles`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `resetLifeVehicles`()
BEGIN
	UPDATE vehicles SET `active`= 0;
END
;;
DELIMITER ;
