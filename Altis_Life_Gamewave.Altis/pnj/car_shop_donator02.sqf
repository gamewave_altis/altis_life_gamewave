/* 
	CarshopLucel
*/
_this enableSimulation false; 
_this allowDamage false; 
_this addAction["<t color='#AAF200'>Concessionnaire Donateur</t>",
life_fnc_vehicleShopMenu,["donator",civilian,["donator_2","donator_2_1"]
,"civ","Concessionnaire Donateur"]];

_this addAction["<t color='#AAF200'>Concessionnaire Taxi</t>",
life_fnc_vehicleShopMenu,["taxi_shop",civilian,["donator_2","donator_2_1"]
,"taxi","Concessionnaire Automobile"],90,false,false,"",'playerSide == civilian && license_civ_taxi'];  

_this addAction["<t color='#AAF200'>Concessionnaire Dépanneur</t>",
life_fnc_vehicleShopMenu,["dep_shop",civilian,["donator_2","donator_2_1"]
,"dep","Concessionnaire Automobile"],90,false,false,"",'playerSide == civilian && license_civ_dep'];  

_this addAction["<t color='#AAF200'>Concessionnaire Médecin</t>",
life_fnc_vehicleShopMenu,["med_shop",civilian,["donator_2","donator_2_1"]
,"medic","Concessionnaire Automobile"],90,false,false,"",'playerSide == civilian && license_civ_medic'];  

_this addAction["<t color='#AAF200'>Concessionnaire Mercenaire</t>",
life_fnc_vehicleShopMenu,["merc_v",civilian,["donator_2","donator_2_1"]
,"merc","Concessionnaire mercenaire"],0,false,false,"",'playerSide == civilian && license_civ_merc'];


_this addAction["<t color='#FF9900'>Garage</t>",
{  [[getPlayerUID player,playerSide,"Car",player],"TON_fnc_getVehicles",false,false] spawn life_fnc_MP;
life_garage_type = "Car";
createDialog "Life_impound_menu";
disableSerialization;
ctrlSetText[2802,"Recherches des véhicules...."];
life_garage_sp = "donator_2";  }];
_this addAction["<t color='#FF9900'>Rentrer au garage</t>",life_fnc_storeVehicle,"",0,false,false,"",'!life_garage_store'];