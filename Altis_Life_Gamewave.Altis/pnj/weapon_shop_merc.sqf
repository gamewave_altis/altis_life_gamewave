_this enableSimulation false; 
_this allowDamage false; 
_this switchMove "Acts_Kore_TalkingOverRadio_in";
removeAllContainers _this;
RemoveAllWeapons _this;
{_this removeMagazine _x;} foreach (magazines _this);
removeUniform _this;
removeVest _this;
removeBackpack _this;
removeGoggles _this;
removeHeadGear _this;
{
	_this unassignItem _x;
	_this removeItem _x;
} foreach (assignedItems _this);

if(hmd _this != "") then {
	_this unlinkItem (hmd _this);
};


_this addAction["<t color='#d3c89f'>Boutique pour mercenaire</t>",life_fnc_virt_menu,"merc_shop",1,false,false,"",'playerSide == civilian && license_civ_merc']; 
_this addAction["<t color='#d3c89f'>Armes</t>",life_fnc_weaponShopMenu,"merc_weapon",0,false,false,"",'playerSide == civilian && license_civ_merc']; 
_this addAction["<t color='#d3c89f'>Munitions</t>",life_fnc_weaponShopMenu,"merc_ammo",0,false,false,"",'playerSide == civilian && license_civ_merc']; 
_this addAction["<t color='#d3c89f'>Equipements</t>",life_fnc_weaponShopMenu,"merc_tools",0,false,false,"",'playerSide == civilian && license_civ_merc']; 
_this addAction["<t color='#d3c89f'>Vêtement</t>",life_fnc_clothingMenu,"merc",-2,false,false,"",'playerSide == civilian && license_civ_merc'];
//_this addAction["<t color='#d3c89f'>Magasin général</t>",life_fnc_weaponShopMenu,"genstore",-2,false,false,"",'playerSide == civilian && license_civ_merc'];
//_this addAction[format["<t color='#00ffff'>Achat: %1</t> <t color='#AAF200'>(%2€)</t>",["license_civ_merc"] call life_fnc_varToStr,[(["merc"] call life_fnc_licensePrice)] call life_fnc_numberText],  life_fnc_buyLicense,"merc",0,false,false,"",' !license_civ_dep && !license_civ_medic && !license_civ_taxi && !license_civ_rebel && !license_civ_merc && playerSide == civilian ']; 
_this addAction["<t color='#AAF200'>Distributeur</t>",life_fnc_atmMenu,"",-5,false,false,"",'playerSide == civilian']; 
_this setVariable["realname", "Boutique mercenaire"];
//_this addaction ["<t color='#d3c89f'>Adopter un chien (10000 €)</t>",{[(_this select 1)] execVM "dogfunctions.sqf"},"",-4,false,false,"",'playerSide == civilian && license_civ_merc'];
  