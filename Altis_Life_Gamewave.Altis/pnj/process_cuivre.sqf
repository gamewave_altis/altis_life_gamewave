/* 
	Lucel
	_null=this execVM "pnj\process_cuivre.sqf";
*/
removeallweapons _this;
_this enableSimulation false; 
_this allowDamage false; 
_this addAction["<t color='#FF9900'>Traitement du cuivre</t>",life_fnc_processAction,"copper",0,false,false,"",' life_inv_copperore > 0 && !life_is_processing'];
_this addAction[format["<t color='#00ffff'>%1</t> <t color='#AAF200'> (%2€) </t>",["license_civ_copper"] call life_fnc_varToStr,[(["copper"] call life_fnc_licensePrice)] call life_fnc_numberText],life_fnc_buyLicense,"copper",0,false,false,"",' !license_civ_copper && playerSide == civilian '];
