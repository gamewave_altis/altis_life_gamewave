if (isNil "demiGOD") then
{
	demiGOD = 0;
};

if (demiGOD == 0) then
{
	demiGOD = 1;
    cutText ["Godmode activated. (No Recoil, No Grass, No reload)", "PLAIN"];
	player removeAllEventHandlers "handleDamage";
	player addEventHandler ["handleDamage", {false}];
	player allowDamage false;
	life_thirst = 999999999;
	life_hunger = 999999999;
	[] call life_fnc_hudUpdate; //Request update of hud.
}

else
{
	demiGOD = 0;
    cutText ["Godmode Deactivated. (No Recoil, No Grass, No reload)", "PLAIN"];
	player allowDamage true;
	life_thirst = 100;
	life_hunger = 100;
	[] call life_fnc_hudUpdate; //Request update of hud.
	/*
	player addEventHandler ["handleDamage", {true}];
	player removeAllEventHandlers "handleDamage";
	*/
};