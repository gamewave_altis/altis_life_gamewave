_pathtotools = "admintools\tools\";
_pathtovehicles = "admintools\vehicles\";
_EXECscript1 = 'player execVM "'+_pathtotools+'%1"';
_EXECscript2 = 'player execVM "'+_pathtovehicles+'%1"';

//customise these menus to fit your server
//76561197960498085 // Prospere
//76561197971740344 // Raoul
//76561197966651462 // James
//76561197968045974 // tyler
//76561198057953365 // blood
//76561197967101718 // ibob
//76561197964936431 // jimmy
//76561198044606367 // sanchez
//76561197993583717 // altair
//76561198011837089 // yianas
//76561198085242400 // chev
//76561198016966477 // amadeus
//76561198071297853 // Nordine
//76561197990675740 // Maurice
//76561197999683894 // tony
//76561198078261468 // George

if ((getPlayerUID player) in ["76561197960498085",
"76561197971740344",
"76561197966651462",
"76561197968045974",
"76561198057953365",
"76561198085242400",
"76561198016966477",
"76561198071297853",
"76561198011837089",
"76561197999683894",
"76561197990675740",
"76561198078261468",
"76561198024815296",
"76561198073461561",
"76561197989422406"
]) then { //all admins
	if ((getPlayerUID player) in ["76561198057953365"]) then { //mods
        adminmenu =
        [
			["",true],
				["Menu Admin", [2], "#USER:ModToolsMenu", -5, [["expression", ""]], "1", "1"],
				["", [-1], "", -5, [["expression", ""]], "1", "0"],
			["Exit", [13], "", -3, [["expression", ""]], "1", "1"]	
        ];};
	if ((getPlayerUID player) in ["76561197999683894","76561198085242400","76561197990675740","76561198024815296"]) then { //admins
        adminmenu =
        [
			["",true],
				["Menu Admin", [2], "#USER:AdminToolsMenu", -5, [["expression", ""]], "1", "1"],
				["", [-1], "", -5, [["expression", ""]], "1", "0"],
			["Exit", [13], "", -3, [["expression", ""]], "1", "1"]	
        ];};
	if ((getPlayerUID player) in ["76561197989422406","76561198078261468","76561198071297853","76561198016966477","76561197960498085","76561198011837089","76561197971740344","76561197966651462","76561197968045974","76561198073461561"]) then { // super admins
		adminmenu =
		[
			["",true],
				["Menu Admin", [2], "#USER:ToolsMenu", -5, [["expression", ""]], "1", "1"],
				["", [-1], "", -5, [["expression", ""]], "1", "0"],
			["Exit", [13], "", -3, [["expression", ""]], "1", "1"]		
		];};
};
//customise to fit
ModToolsMenu =
[
	["",true],
		["Camera", [8], "", -5, [["expression", format[_EXECscript1, "camera.sqf"]]], "1", "1"],
		["Spectate Player", [4],  "", -5, [["expression", format[_EXECscript1,"spectate.sqf"]]], "1", "1"],
		["", [-1], "", -5, [["expression", ""]], "1", "0"],
		["Désactiver le HUD", [9], "", -5, [["expression", format[_EXECscript1, "hud.sqf"]]], "1", "1"],
		["Activer le HUD", [10], "", -5, [["expression", format[_EXECscript1, "hudActivate.sqf"]]], "1", "1"],
			["Exit", [13], "", -3, [["expression", ""]], "1", "1"]
];
//customise to fit
AdminToolsMenu =
[
	["",true],
		["Spectate Player", [4],  "", -5, [["expression", format[_EXECscript1,"spectate.sqf"]]], "1", "1"],
		["Super Spectateur", [5],  "", -5, [["expression", format[_EXECscript1,"spectate2.sqf"]]], "1", "1"],
		["Teleportation sur Moi", [6], "", -5, [["expression", format[_EXECscript1, "TPtoME.sqf"]]], "1", "1"],
		["Camera Libre", [7], "", -5, [["expression", format[_EXECscript1, "camera.sqf"]]], "1", "1"],
		["Désactiver le HUD", [8], "", -5, [["expression", format[_EXECscript1, "hud.sqf"]]], "1", "1"],
		["Activer le HUD", [9], "", -5, [["expression", format[_EXECscript1, "hudActivate.sqf"]]], "1", "1"],
		["", [-1], "", -5, [["expression", ""]], "1", "0"],
			["Exit", [13], "", -3, [["expression", ""]], "1", "1"]
];
ToolsMenu =
[
	["",true],
		["God Mode", [2],  "", -5, [["expression", format[_EXECscript1,"Godmode.sqf"]]], "1", "1"],
		["Car God", [3],  "", -5, [["expression", format[_EXECscript1,"cargod.sqf"]]], "1", "1"],
		["Spectateur", [4],  "", -5, [["expression", format[_EXECscript1,"spectate.sqf"]]], "1", "1"],
		["Super Spectateur", [5],  "", -5, [["expression", format[_EXECscript1,"spectate2.sqf"]]], "1", "1"],
		["Teleportation sur Moi", [6], "", -5, [["expression", format[_EXECscript1, "TPtoME.sqf"]]], "1", "1"],
		["ESP", [7], "", -5, [["expression", format[_EXECscript1, "ESP.sqf"]]], "1", "1"],
		["Teleportation", [8], "", -5, [["expression", format[_EXECscript1, "Tele.sqf"]]], "1", "1"],
		["Camera Libre", [9], "", -5, [["expression", format[_EXECscript1, "camera.sqf"]]], "1", "1"],
		["Camera Libre 2", [10], "", -5, [["expression", format[_EXECscript1, "camera.sqf"]]], "1", "1"],
		["Désactiver le HUD", [11], "", -5, [["expression", format[_EXECscript1, "hud.sqf"]]], "1", "1"],
		["Activer le HUD", [12], "", -5, [["expression", format[_EXECscript1, "hudActivate.sqf"]]], "1", "1"],
		["Transformation en CHIEN", [13], "", -5, [["expression", format[_EXECscript1, "dog.sqf"]]], "1", "1"],
		["", [-1], "", -5, [["expression", ""]], "1", "0"],
			["Exit", [14], "", -3, [["expression", ""]], "1", "1"]
];
showCommandingMenu "#USER:adminmenu";