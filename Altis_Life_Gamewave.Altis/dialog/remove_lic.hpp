#define GUI_GRID_X	(0)
#define GUI_GRID_Y	(0)
#define GUI_GRID_W	(0.025)
#define GUI_GRID_H	(0.04)
#define GUI_GRID_WAbs	(1)
#define GUI_GRID_HAbs	(1)
////////////////////////////////////////////////////////
// GUI EDITOR OUTPUT START (by Prospere Lucel, v1.063, #Zyvoga)
////////////////////////////////////////////////////////
class licenseRemove {

	idd = 8888;
	name = "licenseRemove";
	movingEnable = true;
	enableSimulation = true;
	
class controlsBackground {
	
	class MainBackground: Life_RscText
	{
		idc = -1;

		x = 12.5 * GUI_GRID_W + GUI_GRID_X;
		y = 5.8 * GUI_GRID_H + GUI_GRID_Y;
		w = 13.5 * GUI_GRID_W;
		h = 13 * GUI_GRID_H;
		colorBackground[] = {0,0,0,0.7};
	};
	class itemHeader: Life_RscText
	{
		idc = -1;

		text = "Permis :"; 
		x = 12.5 * GUI_GRID_W + GUI_GRID_X;
		y = 5.8 * GUI_GRID_H + GUI_GRID_Y;
		w = 13.5 * GUI_GRID_W;
		h = 1 * GUI_GRID_H;
		colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])","(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])","(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])",0.5};
	};
};

class controls {

	class licenseList: Life_RscListBox
	{
		idc = 2005;
			text = "";
			sizeEx = 0.04;
		x = 13.5 * GUI_GRID_W + GUI_GRID_X;
		y = 7 * GUI_GRID_H + GUI_GRID_Y;
		w = 11.5 * GUI_GRID_W;
		h = 10 * GUI_GRID_H;
	};
	class RemoveButton: Life_RscButtonMenu
	{
		onButtonClick = "[] call life_fnc_licensesremoveAction;";

		idc = 1016;
		text = "Retirer"; //--- ToDo: Localize;
		x = 13.32 * GUI_GRID_W + GUI_GRID_X;
		y = 17.52 * GUI_GRID_H + GUI_GRID_Y;
		w = 11.8343 * GUI_GRID_W;
		h = 1 * GUI_GRID_H;
		colorText[] = {1,1,1,1};
		colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])","(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])","(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])",0.5};
	};
	class ButtonClose : Life_RscButtonMenu {
			idc = 2013;
			text = "Fermer";
			onButtonClick = "closeDialog 0;";
	x = 12.5 * GUI_GRID_W + GUI_GRID_X;
	y = 18.8 * GUI_GRID_H + GUI_GRID_Y;
	w = 13.5 * GUI_GRID_W;
	h = 1 * GUI_GRID_H;
		};
	};
};
////////////////////////////////////////////////////////
// GUI EDITOR OUTPUT END
////////////////////////////////////////////////////////
