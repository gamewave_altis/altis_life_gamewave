/*
	File: fn_bankDeposit.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Figure it out.
*/
if(playerSide == civilian) exitWith {hint "Vous ne pouvez pas déposer cette argent en tant que Civil ! Pas d'argent sale ici !"};
private["_value"];
_value = parseNumber(ctrlText 2702);

//Series of stupid checks
if(_value > 999999) exitWith {hint localize "STR_ATM_GreaterThan";};
if(_value < 0) exitWith {};
if(!([str(_value)] call life_fnc_isnumeric)) exitWith {hint localize "STR_AIM_notnumeric"};
if(_value > life_flouze) exitWith {hint "Tu n'as pas tant d'argent sur toi !"};

life_flouze = life_flouze - _value;
life_epargne = life_epargne + _value;

hint format["Vous avez déposé %1€ dans votre compte en banque.",[_value] call life_fnc_numberText];
[] call life_fnc_epargneMenu;
[] call SOCK_fnc_updateRequest; //Silent Sync