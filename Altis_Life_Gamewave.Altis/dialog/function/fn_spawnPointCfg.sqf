/*
	File: fn_spawnPointCfg.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Master configuration for available spawn points depending on the units side.
	
	Return:
	[Spawn Marker,Spawn Name,Image Path]
*/
private["_side","_ret","_spawnList","_mk","_mkName"];
_side = [_this,0,civilian,[civilian]] call BIS_fnc_param;
_spawnList = [];

//Spawn Marker, Spawn Name, PathToImage
switch (_side) do
{
	case west:
	{
	if(!cop_jail_spawn) then {
	life_cop_respawn_7 = false;
	}else{
	life_cop_respawn_7 = true;
	};
	if(life_cop_respawn_1) then {_spawnList pushBack ["cop_spawn_1","Kavala HQ","\a3\ui_f\data\map\MapControl\watertower_ca.paa"];};
	if(life_cop_respawn_2) then {_spawnList pushBack ["cop_spawn_2","Pyrgos HQ","\a3\ui_f\data\map\MapControl\fuelstation_ca.paa"];};
	if(life_cop_respawn_3) then {_spawnList pushBack ["cop_spawn_3","Athira HQ","\a3\ui_f\data\map\GroupIcons\badge_rotate_0_gs.paa"];};
	if(life_cop_respawn_4) then {_spawnList pushBack ["cop_spawn_4","Air HQ","\a3\ui_f\data\map\Markers\NATO\b_air.paa"];};
	if(life_cop_respawn_5) then {_spawnList pushBack ["cop_spawn_5","Sofia HQ","\a3\ui_f\data\map\GroupIcons\badge_rotate_0_gs.paa"];};
	if(life_cop_respawn_6) then {_spawnList pushBack ["cop_spawn_6","Camp d'entrainement","\a3\ui_f\data\map\MapControl\watertower_ca.paa"];};
	if(life_cop_respawn_7) then {_spawnList pushBack ["cop_spawn_7","Prison d'Altis","\a3\ui_f\data\map\MapControl\watertower_ca.paa"];};
	};                                               
	
	case civilian:
	{
		//if have reb license, only spawn here
		if(license_civ_rebel && playerSide == civilian) then {
		if(life_reb_respawn_1) then {_spawnList pushBack ["reb_spawn_1","Camp Katalaki","\a3\ui_f\data\map\MapControl\watertower_ca.paa"];};
		if(life_reb_respawn_2) then {_spawnList pushBack ["reb_spawn_2","Camp Frini","\a3\ui_f\data\map\MapControl\watertower_ca.paa"];};
		if(life_reb_respawn_3) then {_spawnList pushBack ["reb_spawn_3","Camp Iraklia","\a3\ui_f\data\map\MapControl\watertower_ca.paa"];};
		if(life_reb_respawn_4) then {_spawnList pushBack ["reb_spawn_4","Camp Sfaka","\a3\ui_f\data\map\MapControl\watertower_ca.paa"];};
		};

		if(license_civ_merc && playerSide == civilian) then {
		if(life_merc_respawn_1) then {_spawnList pushBack ["merc_spawn_1","Camp Mercenaire","\a3\ui_f\data\map\MapControl\watertower_ca.paa"];};
		if(life_merc_respawn_2) then {_spawnList pushBack ["merc_spawn_2","Bureau Mercenaire","\a3\ui_f\data\map\MapControl\watertower_ca.paa"];};
		};
		
		if(!license_civ_rebel && playerSide == civilian) then {
		if(life_civ_respawn_1) then {_spawnList pushBack ["civ_spawn_1","Kavala","\a3\ui_f\data\map\MapControl\watertower_ca.paa"];};
		if(life_civ_respawn_2) then {_spawnList pushBack ["civ_spawn_2","Pyrgos","\a3\ui_f\data\map\MapControl\watertower_ca.paa"];};
		if(life_civ_respawn_3) then {_spawnList pushBack ["civ_spawn_3","Athira","\a3\ui_f\data\map\MapControl\watertower_ca.paa"];};
		if(life_civ_respawn_4) then {_spawnList pushBack ["civ_spawn_4","Sofia","\a3\ui_f\data\map\MapControl\watertower_ca.paa"];};
		if(life_civ_respawn_5) then {_spawnList pushBack ["civ_spawn_5","Neochori","\a3\ui_f\data\map\MapControl\watertower_ca.paa"];};	
		};		
		
			/*
			_houses = [
						"Land_i_House_Small_01_V1_F",
						"Land_i_House_Small_01_V2_F",
						"Land_i_House_Small_01_V3_F",
						"Land_i_House_Small_02_V1_F",
						"Land_i_House_Small_02_V2_F",
						"Land_i_House_Small_02_V3_F",
						"Land_i_House_Small_03_V1_F",
						"Land_i_House_Big_01_V1_F",
						"Land_i_House_Big_01_V2_F",
						"Land_i_House_Big_01_V3_F",
						"Land_i_House_Big_02_V1_F",
						"Land_i_House_Big_02_V2_F",
						"Land_i_House_Big_02_V3_F",
						"Land_i_Addon_02_V1_F",
						"Land_i_Addon_03_V1_F",
						"Land_i_Addon_03mid_V1",
						"Land_i_Addon_04_V1_F",
						"Land_i_Garage_V1_F",
						"Land_i_Garage_V2_F",
						"Land_i_Garage_V1_dam_F"
						];

						_i = 1;
						{
						_house = nearestObject [(_x select 0), "House_F"];
						if((typeOf _house) in _houses) then {

						_mkName = format["civ_spawn_home_%1", _i];

						if (isNil (_mkName)) then {
						_mk = createMarkerLocal [_mkName, (_x select 0)];
						_mk setMarkerAlphaLocal 0;
						};

						_spawnList set [count _spawnList, [_mkName, format ["Maison %1", _i], "\a3\ui_f\data\map\MapControl\watertower_ca.paa"]];
						_i = _i + 1;
						};
						}forEach life_houses; 
			*/		
		if(count life_houses > 0) then {
			{
				_pos = call compile format["%1",_x select 0];
				_house = nearestBuilding _pos;
				_houseName = getText(configFile >> "CfgVehicles" >> (typeOf _house) >> "displayName");
				
                _spawnList pushBack [format["house_%1",_house getVariable "uid"],_houseName,"\a3\ui_f\data\map\MapControl\lighthouse_ca.paa"];

			} foreach life_houses;
		};	
	};
	
};
_spawnList;
