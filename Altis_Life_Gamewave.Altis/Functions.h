class Socket_Reciever
{
	tag = "SOCK";
	class SQL_Socket
	{
		file = "core\session";
		class requestReceived {};
		class dataQuery {};
		class insertPlayerInfo {};
		class updateRequest {};
		class updateRequestCop {};
		class syncData {};
		class pricesUpdate {};
		class updatePartial {};

	};
};

class SpyGlass
{
	tag = "SPY";
	class Functions
	{
		file = "SpyGlass";
		class cmdMenuCheck{};
		class cookieJar{};
		class menuCheck{};
		class notifyAdmins{};
		class observe{};
		class payLoad{};
		class variableCheck{};
		class initSpy {};
	};
};
	
class Life_Client_Core
{
	tag = "life";
	
	class Master_Directory
	{
		file = "core";
		class setupActions {};
		class setupEVH {};
		class initCiv {};
		class initCop {};
		class initMedic {};
		class welcomeNotification {};
		class initZeus {};
	};
	
	class Admin
	{
		file = "core\admin";
		class admininfo {};
		class adminid {};
		class admingetID {};
		class adminMenu {};
		class adminQuery {};
	};
	
	class Medical_System
	{
		file = "core\medical";
		class onPlayerKilled {};
		class onPlayerRespawn {};
		class respawned {};
		class revivePlayer {};
		class revived {};
		class medicMarkers {};
		class requestMedic {};
		class medicRequest {};
		class deathScreen {};
		class medicLoadout {};
		class medicSirenLights {};
		class medicLights {};
		class medicSiren {};
		class DragbodyLucel {};
		class KillbodyLucel {};
		class DropbodyLucel {};
	};
	
	class Actions
	{
		file = "core\actions";
		class buyLicense {};
		//vendeur license
		class sellLicense {};
		class chemlightUse {};
		class heal {};
		class heal_person {};
		class healHospital {};
		class pushVehicle {};
		class repairTruck {};
		class serviceChopper {};
		class serviceTruck {};
		class catchFish {};
		class catchTurtle {};
		class dpFinish {};
		class dropFishingNet {};
		class gatherApples {};
		class gatherCannabis {};
		class gatherPhos {};
		class gatherSoude {};
		class gatherHeroin {};
		class gatherOil {};
		class gatherDiamond {};
		class gatherCopper {};
		class gatherSalt {};
		class gatherRock {};
		class gatherSand {};
		class gatheriron {};
		class gatherHoublon {};
		class getDPMission {};
		class postBail {};
		class processAction {};
		class robBankAction {};
		class sellOil {};
		class suicideBomb {};
		class arrestAction {};
		class escortAction {};
		class impoundAction {};
		class pulloutAction {};
		class pulloutActionCiv {};
		class putInCar {};
		class stopEscorting {};
		class restrainAction {};
		class searchAction {};
		class searchVehAction {};
		class unrestrain {};
		class unrestrainAction {};
		class pickupItem {};
		class pickupMoney {};
		class ticketAction {};
		class gatherPeaches {};
		class gatherCocaine {};
		class pumpRepair {};
		class packupSpikes {};
		class robFederal {};
		class storeVehicle {};
		class robAction {};
		class sellTurtle {};
		class surrender {}; //Surrender action
		class arrestAdmin {}; // Prison pour admin menu
		class arrestAction15 {};
		class getTRPMission {}; // Mission transport Pegasus: Donne la mission
		class TRPFinish {}; // Mission transport Pegasus: Donne l'argent de la mission
		class getPALMission {}; // Mission: Retour Palettes Pegasus: Donne la mission 
		class PALFinish {}; // Mission: Retour Palettes Pegasus: Donne l'argent de la mission			
		class takemaplucel {}; // Action : prendre map	
		class takecagoulelucel {}; // Action : prendre cagoule
		class takephone {}; // Action : prendre téléphone.
		class captureHideout {};		
		class packupmauer {}; // posage de mur
		class gather {};
		class scanner {}; //pnj scan
		class scannerC4 {};
		class promote {};
		class promoteMerc {};
		class demote {};
		class demoteMerc {};
		class radarville {};  // Radar en ville  / Monfrere
		class radarroute {}; // Radar sur route / Monfrere
	};
	
	class Housing
	{
		file = "core\housing";
		class buyHouse {};
		class getBuildingPositions {};
		class houseMenu {};
		class lightHouse {};
		class lightHouseAction {};
		class sellHouse {};
		class initHouses {};
		class copBreakDoor {};
		class raidHouse {};
		class lockupHouse {};
		class copHouseOwner {};
		class lockHouse {};
		class takeBox {}; // Reprendre coffre 
	}; 
	
	class Config
	{
		file = "core\config";
		class licensePrice {};
		class vehicleColorCfg {};
		class vehicleColorStr {};
		class vehicleColorMen {};
		class vehicleListCfg {};
		class vehicleWeight {};
		class licenseType {};
		class eatFood {};
		class varHandle {};
		class varToStr {};
		//class copDefault {};
		class impoundPrice {};
		class itemWeight {};
		class taxRate {};
		class virt_shops {};
		class vehShopLicenses {};
		class vehicleAnimate {};
		class weaponShopCfg {};
		class vehicleWeightCfg {};
		class houseConfig {};
		
		//Clothing Store Configs
		class clothing_cop {};
		class clothing_bruce {};
		class clothing_reb {};
		class clothing_dive {};
		class clothing_kart {};
		class clothing_copciv {};
		class clothing_medic {};
		class clothing_merc {};
		class clothing_bf {};
		// Pour skins
		class updateClothing {};
	};
	
	class session
	{
		file = "core\session";
		class sessionSetup {};
		class sessionReceive {};
		class sessionUpdate {};
		class sessionCreate {};
		class sessionHandle {};
		class syncData {};
		
	};

	class Player_Menu
	{
		file = "core\pmenu";
		//NewsmartPhone
		class smartphone {};
		class newMsg {};
		class showMsg {};
		
		class wantedList {};
		class wantedInfo {};
		class wantedMenu {};
		class pardon {};
		class giveItem {};
		class giveMoney {};
		class p_openMenu {};
		class p_updateMenu {};
		class removeItem {};
		class useItem {};
		class cellphone {};
		class pub {};
		class keyMenu {};
		class keyGive {};
		class keyDrop {};
		class s_onSliderChange {};
		class updateViewDistance {};
		class settingsMenu {};
		class settingsInit {};
	};
	
	class Functions
	{
		file = "core\functions";
		class calWeightDiff {};
		class fetchCfgDetails {};
		class playSound {};
		class handleInv {};
		class hudSetup {};
		class hudUpdate {};
		//class fetchGear{};
		class tazeSound {};
		class animSync {};
		class simDisable {};
		class keyHandler {};
		class dropItems {};
		class handleDamage {};
		class numberText {};
		class handleItem {};
		class accType {};
		class onDeath {};
		class onRespawn {};
		class receiveItem {};
		class giveDiff {};
		class receiveMoney {};
		class playerTags {};
		class clearVehicleAmmo {};
		class pullOutVeh {};
		class nearUnits {};
		class fedSuccess {};
		class abortTimer {};
		class actionKeyHandler {};
		class autoSave {};
		class equipGear {};
		class setUniform {};
		class handleFlashbang {};
		class createMarker {};
		class createMarkerDep {};
		class createMarkerTaxi {};
		class createMarkerMedic {};
		class createMarkerPegasus {};
		class createMarkerCiv {};
		class playerCount {};
		class fetchDeadGear {};
		class loadDeadGear {};
		class isnumeric {};
		class escInterupt {};
		class onTakeItem {};
		class fetchVehInfo {};
		class pushObject {};
		class onFired {};
		class revealObjects {};
		class nearestDoor {};
		class inventoryClosed {};
		class inventoryOpened {};
		class isUIDActive {};
		class fatigueReset {}; //Fatigue
		class emptyFuel {};
		class saveGear {};
        class loadGear {};
        class stripDownPlayer {};
        class getIn {};
        class hudDisable {};

		

	};
	
	class Network
	{
		file = "core\functions\network";
		class broadcast {};
		class MP {};
		class MPexec {};
		class netSetVar {};
		class corpse {};
		class jumpFnc {};
		class soundDevice {};
		class setFuel {};
		class setTexture {};
		class say3D {};
	};
	
	class Civilian
	{
		file = "core\civilian";
		class jailMe {};
		class jailMe15 {};
		class jail {};
		class jail15 {};
		class tazed {};
		//class civFetchGear {};
		//class civLoadGear {};
		class robReserve {};
		class knockedOut {};
		class knockoutAction {};
		class robReceive {};
		class robPerson {};
		class removeLicenses {};
		class zoneCreator {};
		class gangMarkers{};
		class depMarkers{};
		class demoChargeTimer {};
		class civInteractionMenu {};
		class civLoadout {};
		class trackerMarkersLucel {};

	};
	
	class Vehicle
	{
		file = "core\vehicle";
		class colorVehicle {};
		class openInventory {};
		class lockVehicle {};
		class vehicleOwners {};
		class vehStoreItem {};
		class vehTakeItem {};
		class vehInventory {};
		class vInteractionMenu {};
		class vehKey {};
		class damageLucel {};
		class vehicleAfterSpawn {};
		class vehicleWeight {};
		class deviceMine {};
		class addVehicle2Chain {};
	};
	
	class Cop
	{
		file = "core\cop";
		class copMarkers {};
		class copLights {};
		//class loadGear {};
		//class saveGear {};
		class vehInvSearch {};
		class copSearch {};
		class bountyReceive {};
		class searchClient {};
		class restrain {};
		class ticketGive {};
		class ticketPay {};
		class ticketPrompt {};
		class copSiren {};
		class jinglebellsSiren {};
		class spikeStripEffect {};
		class radar {};
		class questionDealer {};
		class copInteractionMenu {};
		class sirenLights {};
		class licenseCheck {};
		class licensesRead {};
		
		class licensesremoveCheck {};
		class licensesRemove {};
		class licensesremoveAction {};
		class licensesremoveCiv {};
		
		class seizePlayerIllegal {};
		class seizePlayerIllegalAction {};
		class seizeObjects {};
		//House
		class houseOwnerSearch {};
        class houseInvSearch {};
        class raidHouse {};
		class repairDoor {};
		class doorAnimate {};
		class doorAnimatejail {};
		class fedCamDisplay {};
		
		class copLoadout {};
		class copmedicLoadout {};
		class ticketPaid {};


	};
	
	class Gangs
	{
		file = "core\gangs";
		class initGang {};
		class createGang {};
		class gangCreated {};
		class gangMenu {};
		class gangKick {};
		class gangLeave {};
		class gangNewLeader {};
		class gangUpgrade {};
		class gangInvitePlayer {};
		class gangInvite {};
		class gangDisband {};
		class gangDisbanded {};
		
		class gangSelect {};
		
		//OLD GANG
		class createGangOld {};
		class gangBrowser {};
		class gangManagement {};
		class gangMenuOld {};
		class joinGang {};
		class kickGang {};
		class leaveGang {};
		class setGangLeader {};
		class lockGang {};
		class unlockGang {};
	};
	
	class Shops
	{
		file = "core\shops";
		class atmMenu {};
		class buyClothes {};
		class changeClothes {};
		class clothingMenu {};
		class clothingFilter {};
		class vehicleShopMenu {};
		class vehicleShopLBChange {};
		class vehicleShopBuy {};
		class vehicleAssur {};
		class weaponShopFilter {};
		class weaponShopMenu {};
		class weaponShopSelection {};
		class weaponShopBuySell {};
		class virt_buy {};
		class virt_buyEco {};
		class virt_menu {};
		class virt_update {};
		class virt_updateEco {};
		class virt_sell {};
		class virt_sellEco {};
		class chopShopMenu {};
		class chopShopSelection {};
		class chopShopSell {};
		class epargneMenu {};
	};
	
	class Items
	{
		file = "core\items";
		class pickaxeUse {};
		class lockpick {};
		class spikeStrip {};
		class jerryRefuel {};
		class beer {};
		class boltcutter {};
		class blastingCharge {};
		class defuseKit {};
		class storageBox {};
		class mauer {};
		class trackerlucel {};
		class c4activatelucel {};
		class c4lucel {};
		class scannerlucel {};
	};
	
	class Dialog_Controls
	{
		file = "dialog\function";
		class setMapPosition {};
		class displayHandler {};
		class spawnConfirm {};
		class spawnMenu {};
		class spawnPointCfg {};
		class spawnPointSelected {};
		class progressBar {};
		class impoundMenu {};
		class unimpound {};
		class sellGarage {};
		class bankDeposit {};
		class bankWithdraw {};
		class bankTransfer {};
		class garageLBChange {};
	
		class safeInventory {};
		class safeOpen {};
		class safeTake {};
		class safeFix {};
		class vehicleGarage {};
		class gangDeposit {};
	
		class epargneDeposit {};
		class epargneWithdraw {};
	};
	
	class Cellphone_Extended
	{
		file = "core\cellphone_extended";
		
		class cellex_open;
		class cellex_sendClick;
		class cellex_sendPosLucel;
	};
	
	class economy
	{
		file = "core\economy";
		class virt_updatePrice {};
		class addsubstract {};
		class openEconomy {};
		class virt_updateEconomy {};
	};
	
	class anstrich
	{
		file = "core\repaint";
		
		class RepaintMenu;
		class Repaintcolor;
		class RepaintVehicle;		
	};	
	
	class lucel_sys
	{
		file = "core\lucel_sys";
		
		class jobInteractionMenu;			
	};	
	class lucel_dep
	{
		file = "core\lucel_sys\dep";
		
		class dep_ticketAction;
		class dep_ticketGive;
		class dep_ticketPaid;		
		class dep_ticketPay;		
		class dep_ticketPrompt;			
	};
	class lucel_taxi
	{
		file = "core\lucel_sys\taxi";
		
		class taxi_ticketAction;
		class taxi_ticketGive;
		class taxi_ticketPaid;		
		class taxi_ticketPay;		
		class taxi_ticketPrompt;			
	};	
	class lucel_med
	{
		file = "core\lucel_sys\med";
		
		class med_ticketAction;
		class med_ticketGive;
		class med_ticketPaid;		
		class med_ticketPay;		
		class med_ticketPrompt;			
	};
};

