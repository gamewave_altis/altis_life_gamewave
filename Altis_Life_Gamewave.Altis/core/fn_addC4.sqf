#include <macro.h>
/*
	based on havena addkey
	
	Altislife.fr
	Tirroire
*/
sleep 3;
private["_vehicleClass","_index","_owners","_uid"];
_uid = getPlayerUID player;
{
	_vehicleClass = getText(configFile >> "CfgVehicles" >> (typeOf _x) >> "vehicleClass");
	if(_vehicleClass in ["Car","Support","Air","Ship","Armored","Submarine"]) then
	{
		_index = -1;
		_owners = _x getVariable ["vehicle_info_c4",[]];
		sleep 0.1;
		for "_i" from 0 to ((count _owners) - 1) do {
			if (isnil {((_owners select _i) select 0)} ) then {
				diag_log format["[fn_vehKey][%2] %1 ", _owners,_uid];
			}
			else
			{
				if(((_owners select _i) select 0) == _uid) then {_index = _i;};
			};
		};
		
		if((_index > -1 ) && !(_x in life_vehicles_c4ed)) then
		{
			life_vehicles_c4ed pushBack _x;
		};
		if((count life_vehicles_c4ed > 0)) then 
		{
			player setVariable["c4",true];
		};
	};
} foreach vehicles;