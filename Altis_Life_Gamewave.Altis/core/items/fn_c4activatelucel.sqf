/*
	ActivateC4Lucel
	DESSCOUBIDOUBIDOU
	altislife.fr
*/
private["_curTarget","_isVehicle","_c4array","_bomberList"];
if((player getVariable "restrained")||(player getVariable "Escorting")||(player getVariable "Surrender")) exitWith {hint "Vous devez avoir les mains libres";};
_c4array = life_vehicles_c4ed;
_pid = getPlayerUID player;
//SI TABEAU INEXISTANT
[[player,"c4_off",20],"life_fnc_playSound",true,false] spawn life_fnc_MP;
if(isNil "_c4array")exitWith{diag_log format ["ISNIL C4ARRAY"];
hint parseText format["<t size='1.5'><t color='#FF0000'>Signal perdu</t></t> <br/><t size='1'>...</t>"];
player setVariable["c4",false];
};
diag_log format [" life_vehicles_c4ed: %1", life_vehicles_c4ed];
diag_log format [" _c4array: %1", _c4array];

// CHECK SI VEH AU GARAGE / DELETE LES NULLOBJECT DU TABLEAU DU FION
{	
	if(isNull _x) then {
	diag_log format ["ISNULL CURTARGET"];
	hint parseText format["<t size='1.5'><t color='#FF0000'>Signal perdu</t></t> <br/><t size='1'>...</t>"];
	life_vehicles_c4ed = life_vehicles_c4ed - [_x];
	};	
	_bomberList = _x getVariable "vehicle_info_c4";
	if(!([_pid,name player] in _bomberList)) then {
	life_vehicles_c4ed = life_vehicles_c4ed - [_x];
	hint parseText format["<t size='1.5'><t color='#FF0000'>Signal perdu</t></t> <br/><t size='1'>...</t>"];
	};
} foreach _c4array;

{
	if(isNull _x) exitWith {};
	if(player distance _x > 700) exitWith {
	hint parseText format["<t size='1.5'><t color='#FF0000'>Signal faible</t></t> <br/><t size='1'>Vous devez être a moins de 700m du véhicule.</t>"];
	};
	[[player,"c4_on",20],"life_fnc_playSound",true,false] spawn life_fnc_MP;
	sleep 4;
	hint parseText format["<t size='1.5'><t color='#FF0000'>Déclenchement réussi</t></t> <br/><t size='1'>...</t>"];
	_x setDamage 1;
	[[0,format["%1 active un C4",name player]],"life_fnc_broadcast",true,false] spawn life_fnc_MP;
	cutText ["Boom !", "PLAIN"];
	life_vehicles_c4ed = life_vehicles_c4ed - [_x];
}forEach life_vehicles_c4ed;


diag_log format ["END life_vehicles_c4ed: %1", life_vehicles_c4ed];

if (count life_vehicles_c4ed == 0) then {

player setVariable["c4",false];

};
