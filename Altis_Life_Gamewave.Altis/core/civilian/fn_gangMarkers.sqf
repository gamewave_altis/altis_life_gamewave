/*
	File: fn_gangMarkers.sqf
	Original file: fn_copMarkers.sqf
	Author: Bryan "Tonic" Boardwine
	Description:
	Shows Gangmembers on map, just like the map for cops does
*/
private["_markers","_gangMembers","_index"];
_markers = [];
_gangMembers = [];


sleep 0.5;
if(visibleMap) then {
	//Create markers
	{
		_marker = createMarkerLocal [format["%1_marker",_x],visiblePosition _x];
		_marker setMarkerColorLocal "ColorBlue";
		_marker setMarkerTypeLocal "Mil_dot";
		_marker setMarkerTextLocal format["%1", name _x];
	
		_markers pushBack [_marker,_x];
		
	} foreach (units group player);
		
	
	_allUnits = playableUnits;
	_grpMembers = group player;

	while {visibleMap} do // Additional value for anti spying purposes
	{
		{
			private["_marker","_unit"];
			_marker = _x select 0;
			_unit = _x select 1;
			if(!isNil "_unit") then
			{
				if(!isNull _unit) then
				{
					_marker setMarkerPosLocal (visiblePosition _unit);
				};
			};
		} foreach _markers;
		//{
		//if(_x in _grpMembers OR side _x != civilian && isNil {(group _x) getVariable "gang_id"}) then {
		//_allUnits set[_forEachIndex,-1];
		//};
		//} foreach _allUnits;
	if(!visibleMap) exitWith {};
	sleep 0.02;

	};



	{deleteMarkerLocal (_x select 0);} foreach _markers;
	_markers = [];
	_gangMembers = [];
};