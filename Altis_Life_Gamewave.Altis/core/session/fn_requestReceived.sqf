#include <macro.h>
/*
	File: fn_requestReceived.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Called by the server saying that we have a response so let's 
	sort through the information, validate it and if all valid 
	set the client up.
*/
life_session_tries = life_session_tries + 1;
if(life_session_completed) exitWith {};
if(life_session_tries > 3) exitWith {cutText["There was an error in trying to setup your client.","BLACK FADED"]; 0 cutFadeOut 999999999;};

0 cutText ["Received request from server... Validating...","BLACK FADED"];
0 cutFadeOut 9999999;

//Error handling and  junk..
if(isNil "_this") exitWith {[] call SOCK_fnc_insertPlayerInfo;};
if(typeName _this == "STRING") exitWith {[] call SOCK_fnc_insertPlayerInfo;};
if(count _this == 0) exitWith {[] call SOCK_fnc_insertPlayerInfo;};
if((_this select 0) == "Error") exitWith {[] call SOCK_fnc_insertPlayerInfo;};
if((getPlayerUID player) != _this select 0) exitWith {[] call SOCK_fnc_dataQuery;};

//Lets make sure some vars are not set before hand.. If they are get rid of them, hopefully the engine purges past variables but meh who cares.
if(!isServer && (!isNil "life_adminlevel" OR !isNil "life_coplevel" OR !isNil "life_donator")) exitWith {
    [[profileName,getPlayerUID player,"VariablesAlreadySet"],"SPY_fnc_cookieJar",false,false] spawn life_fnc_MP;
    [[profileName,format["Variables set before client initialization...\nlife_adminlevel: %1\nlife_coplevel: %2\nlife_donator: %3",life_adminlevel,life_coplevel,life_donator]],"SPY_fnc_notifyAdmins",true,false] spawn life_fnc_MP;
    sleep 0.9;
    ["SpyGlass",false,false] execVM "\a3\functions_f\Misc\fn_endMission.sqf";
};



//Parse basic player information.
life_flouze = parseNumber (_this select 2);
life_dabflouze = parseNumber (_this select 3);
__CONST__(life_adminlevel,parseNumber(_this select 4));
__CONST__(life_donator,parseNumber(_this select 5));
//Loop through licenses
if(count (_this select 6) > 0) then {
	{
		missionNamespace setVariable [(_x select 0),(_x select 1)];
	} foreach (_this select 6);
};

switch(__GETC__(life_donator)) do

{

	case 1: {life_paye = life_paye + 750;};

	case 2: {life_paye = life_paye + 1500;};

	case 3: {life_paye = life_paye + 2000;};

};
life_gear = _this select 8;
[] call life_fnc_loadGear;
//Parse side specific information.
switch(playerSide) do {
	case west: {
		__CONST__(life_coplevel,parseNumber(_this select 7));
		//cop_gear = _this select 8;
		//[] spawn life_fnc_loadGear;
		life_blacklisted = _this select 11;
		life_in_rea = _this select 12;
		life_restrained = _this select 17;
		//__CONST__(life_medicLevel,0);
		isAlive = _this select 9;
		if (!isAlive) then {
		life_bidet = [0,0,0];
		life_alive = false;
		life_restrained = false;
		life_in_rea = false;	
		}
        else
        {
			life_bidet = call compile format["%1",(_this select 10)];
			life_bidet = call compile format["%1",life_bidet];
			life_alive = true;
			life_hunger = parseNumber (_this select 13);
			life_thirst = parseNumber (_this select 14);
			life_damage = parseNumber (_this select 15);
			
        };
		life_epargne = parseNumber (_this select 16);
	};
	
	case civilian: {
		life_is_arrested = _this select 7;
		life_in_rea = _this select 11;
		life_restrained = _this select 16;
		//civ_gear = _this select 8;
		//__CONST__(life_medicLevel,0);
		//[] spawn life_fnc_civLoadGear;
		__CONST__(life_coplevel,0);
		life_houses = _this select 17;
		{
			_house = nearestBuilding (call compile format["%1", _x select 0]);
			life_vehicles set[count life_vehicles,_house];
		} foreach life_houses;
	
		
		isAlive = _this select 9;
		if (!isAlive) then {
		life_bidet = [0,0,0];
		life_alive = false;
		life_restrained = false;
		life_in_rea = false;		
		}
        else
        {
			life_bidet = call compile format["%1",(_this select 10)];
			life_bidet = call compile format["%1",life_bidet];
			life_alive = true;
			life_hunger = parseNumber (_this select 12);
			life_thirst = parseNumber (_this select 13);
			life_damage = parseNumber (_this select 14);
        };
		life_epargne = parseNumber (_this select 15);
		
		life_gangData = _This select 18;
        if(count life_gangData != 0) then {
            [] spawn life_fnc_initGang;
        };
		[] spawn life_fnc_initHouses;


	};
	

	
};
//if(getPlayerUID player == "76561197960498085") then {
//life_session_completed = false;
//}else{
life_session_completed = true;
