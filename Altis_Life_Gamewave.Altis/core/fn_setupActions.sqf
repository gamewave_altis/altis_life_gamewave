/*
	File: fn_setupActions.sqf
	
	Description:
	Master addAction file handler for all client-based actions.
	
	Bidouiller par Lecul.
*/
switch (playerSide) do
{
	case west:
	{
	//Barriere
	life_actions = life_actions + [player addAction["<t color='#FF9900'>Reprendre la barrière</t>",life_fnc_packupmauer,"",0,false,false,"",' _mauer = nearestObjects[getPos player,
	["RoadBarrier_small_F"],8] select 0;
	!isNil "_mauer" && !isNil {(_mauer getVariable "item")}']];
	
	//Voyage Rapide
	life_actions = life_actions + [player addAction["<t color='#FF9900'>Voyage rapide</t>",life_fnc_spawnMenu,"",151,false,false,"",'
	((player distance (getMarkerPos "cop_spawn_1") < 10) OR  (player distance (getMarkerPos "cop_spawn_2") < 10) OR (player distance (getMarkerPos "cop_spawn_3") < 10) OR  (player distance (getMarkerPos "cop_spawn_4") < 10) OR  (player distance (getMarkerPos "cop_spawn_5") < 10) OR  (player distance (getMarkerPos "cop_spawn_6") < 10) OR  (player distance (getMarkerPos "cop_spawn_7") < 10))']];

	//Saisir Armes au sol
	life_actions = life_actions + [player addAction["<t color='#AAF200'>Détruire objets</t>",life_fnc_seizeObjects,cursorTarget,0,false,false,"",'((count(nearestObjects [player,["Land_Razorwire_F"],3])>0) || (count(nearestObjects [player,["Land_Suitcase_F"],3])>0) || (count(nearestObjects [player,["WeaponHolder"],3])>0) || (count(nearestObjects [player,["GroundWeaponHolder"],3])>0) || (count(nearestObjects [player,["WeaponHolderSimulated"],3])>0))']];
	
	//Saisir la Cagoulemesboules
	life_actions = life_actions + [player addAction["<t color='#00ffff'>Prendre Cagoule</t>",life_fnc_takecagoulelucel,cursorTarget,9999999,false,false,"",'
	!isNull cursorTarget 
	&& cursorTarget isKindOf "Man"
	&& (isPlayer cursorTarget)
	&& player distance cursorTarget < 3.5 
	&& !(player getVariable "restrained") 
	&& ((cursorTarget getVariable "restrained") 
	|| (cursorTarget getVariable "surrender"))
	&& (headgear cursorTarget == "H_Shemag_olive"
	|| headgear cursorTarget == "H_ShemagOpen_khk"
	|| headgear cursorTarget == "H_Shemag_tan"
	|| headgear cursorTarget == "H_ShemagOpen_tan"
	|| goggles cursorTarget == "G_Balaclava_blk"
	|| goggles cursorTarget == "G_Balaclava_combat"
	|| goggles cursorTarget == "G_Balaclava_oli"
	|| goggles cursorTarget == "G_Bandanna_aviator"
	|| goggles cursorTarget == "G_Bandanna_sport"
	|| goggles cursorTarget == "G_Bandanna_shades")
	&& !(cursorTarget getVariable "Escorting")
	']];
	
	//Saisir téléphone
	life_actions = life_actions + [player addAction["<t color='#00ffff'>Prendre Téléphone</t>",life_fnc_takephone,cursorTarget,9999999,false,false,"",'
	!isNull cursorTarget 
	&& cursorTarget isKindOf "Man"
	&& (isPlayer cursorTarget)
	&& player distance cursorTarget < 3.5 
	&& !(player getVariable "restrained") 
	&& (cursorTarget getVariable "restrained") 		&& !(cursorTarget getVariable "Escorting")
	&& !(cursorTarget getVariable["robbedphone",FALSE])
	']];
	
	
	
	
	
	//Tracker COP
	life_actions = life_actions + [player addAction["<t color='#FF0000'>Poser un traceur</t>",life_fnc_trackerlucel,cursorTarget,9999999,false,false,"",' 
	!isNull cursorTarget 
	&& ((cursorTarget isKindOf "Car") OR (cursorTarget isKindOf "Air") OR (cursorTarget isKindOf "Ship")) 
	&& player distance cursorTarget < 3.5 
	&& speed cursorTarget < 1 
	&& life_inv_trackerlucel > 0']];
	
	//C4_Veh_Lucel
	life_actions = life_actions + [player addAction["<t color='#FF0000'>Poser du C4</t>",life_fnc_c4lucel,cursorTarget,9999999,false,false,"",' 
	!isNull cursorTarget 
	&& ((cursorTarget isKindOf "Car") OR (cursorTarget isKindOf "Air") OR (cursorTarget isKindOf "Ship")) 
	&& player distance cursorTarget < 3.5 
	&& speed cursorTarget < 1 
	&& life_inv_blastingcharge > 0']];	

	//C4_Activate_Lucel
	//life_actions = life_actions + [player addAction["<t color='#FF0000'>!!! Déclencher C4 !!!</t>",life_fnc_c4activatelucel,"",-9999999,false,false,"",' 
	//(player getVariable "c4")']];

	//Scanner C4
	life_actions = life_actions + [player addAction["<t color='#AAF200'>Scanner véhicule</t>",life_fnc_scannerlucel,cursorTarget,9999999,false,false,"",' 
	!isNull cursorTarget 
	&& ((cursorTarget isKindOf "Car") OR (cursorTarget isKindOf "Air") OR (cursorTarget isKindOf "Ship")) 
	&& player distance cursorTarget < 3.5 
	&& speed cursorTarget < 1 
	&& life_inv_scannerlucel > 0']];

	};
	case civilian:
	{
			//Restrain Actions
		life_actions = [player addAction["<t color='#FF0000'>Attacher</t>",life_fnc_restrainAction,cursorTarget,9999999,false,false,"",'(
		(animationState cursorTarget) == "amovpercmstpsnonwnondnon_amovpercmstpssurwnondnon" 
		|| (animationState cursorTarget) == "Incapacitated" 
		|| (animationState cursorTarget) == "AwopPercMstpSgthWrflDnon_End2") 
		&& (currentWeapon player != "") 
		&& !(player getVariable "restrained") 
		&& cursorTarget isKindOf "Man" 
		&& (isPlayer cursorTarget) 
		&& alive cursorTarget 
		&& cursorTarget distance player < 3 
		&& !(cursorTarget getVariable "Escorting") 
		&& !(cursorTarget getVariable "restrained") 
		&& speed cursorTarget < 1 
		&& life_inv_menotte > 0']];
		
		//Rob person
		life_actions = life_actions + [player addAction["<t color='#FF0000'>Voler de l'argent</t>",life_fnc_robAction,"",0,false,false,"",'
		((animationState cursorTarget) == "amovpercmstpsnonwnondnon_amovpercmstpssurwnondnon"
		|| (animationState cursorTarget) == "Incapacitated" 
		|| (animationState cursorTarget) == "AwopPercMstpSgthWrflDnon_End2" ) 
		&& (currentWeapon player != "") 
		&& !isNull cursorTarget && player distance cursorTarget < 3 
		&& isPlayer cursorTarget 
		&& !(cursorTarget getVariable "CopRestrain") 
		&& !(cursorTarget getVariable["robbed",FALSE])']];
		
		//Saisir la carte
		life_actions = life_actions + [player addAction["<t color='#00ffff'>Prendre Carte</t>",life_fnc_takemaplucel,cursorTarget,9999999,false,false,"",'
		!isNull cursorTarget 
		&& cursorTarget isKindOf "Man"
		&& (isPlayer cursorTarget)
		&& player distance cursorTarget < 3.5 
		&& !(cursorTarget getVariable "CopRestrain")
		&& !(player getVariable "restrained") 
		&& (cursorTarget getVariable "restrained") 
		&& !(cursorTarget getVariable "Escorting")
		&& !(cursorTarget getVariable["robbedmap",FALSE])
		']];

		//Saisir la Cagoulemesboules
		life_actions = life_actions + [player addAction["<t color='#00ffff'>Prendre Cagoule</t>",life_fnc_takecagoulelucel,cursorTarget,9999999,false,false,"",'
		!isNull cursorTarget 
		&& cursorTarget isKindOf "Man"
		&& (isPlayer cursorTarget)
		&& player distance cursorTarget < 3.5 
		&& !(cursorTarget getVariable "CopRestrain")
		&& !(player getVariable "restrained") 
		&& ((cursorTarget getVariable "restrained") 
		|| (cursorTarget getVariable "surrender"))
		&& (headgear cursorTarget == "H_Shemag_olive"
		|| headgear cursorTarget == "H_ShemagOpen_khk"
		|| headgear cursorTarget == "H_Shemag_tan"
		|| headgear cursorTarget == "H_ShemagOpen_tan"
		|| goggles cursorTarget == "G_Balaclava_blk"
		|| goggles cursorTarget == "G_Balaclava_combat"
		|| goggles cursorTarget == "G_Balaclava_oli"
		|| goggles cursorTarget == "G_Bandanna_aviator"
		|| goggles cursorTarget == "G_Bandanna_sport"
		|| goggles cursorTarget == "G_Bandanna_shades")
		&& !(cursorTarget getVariable "Escorting")
		']];
		
			//Saisir téléphone
	life_actions = life_actions + [player addAction["<t color='#00ffff'>Prendre Téléphone</t>",life_fnc_takephone,cursorTarget,9999999,false,false,"",'
	!isNull cursorTarget 
	&& cursorTarget isKindOf "Man"
	&& (isPlayer cursorTarget)
	&& player distance cursorTarget < 3.5 
	&& !(cursorTarget getVariable "CopRestrain")
	&& !(player getVariable "restrained") 
	&& (cursorTarget getVariable "restrained") 		&& !(cursorTarget getVariable "Escorting")
	&& !(cursorTarget getVariable["robbedphone",FALSE])
	']];
		
		//Unrest Action
		//life_actions = life_actions + [player addAction["<t color='#AAF200'>Détacher</t>",life_fnc_unrestrain,cursorTarget,9999999,false,false,"",' !isNull cursorTarget && cursorTarget isKindOf "Man" && (isPlayer cursorTarget) && player distance cursorTarget < 3.5 
		//&& !(cursorTarget getVariable "CopRestrain") && !(player getVariable "restrained") && (cursorTarget getVariable "restrained") && !(cursorTarget getVariable "Escorting")']];

		//Escort
		//life_actions = life_actions + [player addAction["<t color='#FF9900'>Escorter</t>",life_fnc_escortAction,[cursorTarget],9999999,false,false,"",' !isNull cursorTarget && cursorTarget isKindOf "Man" && (isPlayer cursorTarget) && alive cursorTarget && cursorTarget distance player < 3.5 && !(cursorTarget getVariable "CopRestrain") && !(player getVariable "restrained") && (cursorTarget getVariable "restrained") && !(cursorTarget getVariable "Escorting")']];
		//life_actions = life_actions + [player addAction["<t color='#FF9900'>Arrêter éscorte</t>",life_fnc_stopEscorting,_unit,9999999,false,false,"",' !isNull cursorTarget && cursorTarget isKindOf "Man" && (isPlayer cursorTarget) && player distance cursorTarget < 3.5 && (cursorTarget getVariable "Escorting")']];

		//Crochetage
		life_actions = life_actions + [player addAction["<t color='#FF0000'>Crochetage des menottes</t>",life_fnc_lockpick,cursorTarget,9999999,false,false,"",' !isNull cursorTarget && cursorTarget isKindOf "Man" && (isPlayer cursorTarget) && player distance cursorTarget < 3.5 
		&& (cursorTarget getVariable "CopRestrain") && (cursorTarget getVariable "restrained") && !(cursorTarget getVariable "Escorting") && speed cursorTarget < 1 && life_inv_lockpick > 0']];
				
		//Tracker
		life_actions = life_actions + [player addAction["<t color='#FF0000'>Poser un traceur</t>",life_fnc_trackerlucel,cursorTarget,9999999,false,false,"",' 
		!isNull cursorTarget 
		&& ((cursorTarget isKindOf "Car") OR (cursorTarget isKindOf "Air") OR (cursorTarget isKindOf "Ship")) 
		&& player distance cursorTarget < 3.5 
		&& speed cursorTarget < 1 
		&& life_inv_trackerlucel > 0']];	

		//C4_Veh_Lucel
		life_actions = life_actions + [player addAction["<t color='#FF0000'>Poser du C4</t>",life_fnc_c4lucel,cursorTarget,9999999,false,false,"",' 
		!isNull cursorTarget 
		&& ((cursorTarget isKindOf "Car") OR (cursorTarget isKindOf "Air") OR (cursorTarget isKindOf "Ship")) 
		&& player distance cursorTarget < 3.5 
		&& speed cursorTarget < 1 
		&& life_inv_blastingcharge > 0']];	

		//C4_Activate_Lucel
		//life_actions = life_actions + [player addAction["<t color='#FF0000'>!!! Déclencher C4 !!!</t>",life_fnc_c4activatelucel,"",-9999999,false,false,"",' 
		//(player getVariable "c4")']];
		
		//Drop fishing net
		life_actions = [player addAction["<t color='#AAF200'>Jeter le filet pour pêcher</t>",life_fnc_dropFishingNet,"",0,false,false,"",'
		(surfaceisWater (getPos vehicle player)) && (vehicle player isKindOf "Ship") && life_carryWeight < life_maxWeight && speed (vehicle player) < 2 && speed (vehicle player) > -1 && !life_net_dropped ']];
		
		//Put in car	
	//life_actions = life_actions + [player addAction["<t color='#FF9900'>Mettre dans le vehicule</t>",life_fnc_putInCar,_unit,9999999,false,false,"",' !isNull cursorTarget 
	//&& (player distance cursorTarget) < 6  
	//&& speed cursorTarget < 2 
	//&& (cursorTarget isKindOf "Car" 
	//|| cursorTarget isKindOf "Air" 
	//|| cursorTarget isKindOf "Ship")
	//&& player getVariable "currentlyEscorting"
	//']];
		
	////Pull out of car
	//life_actions = life_actions + [player addAction["<t color='#FF9900'>Sortir du vehicule</t>",life_fnc_pulloutAction,cursorTarget,9999999,false,false,"",'!isNull cursorTarget 
	//&& (player distance cursorTarget) < 4 
	//&& (currentWeapon player == primaryWeapon player OR currentWeapon player == handgunWeapon player) 
	//&& currentWeapon player != "" 
	//&& (locked cursorTarget == 0) 
	//&& (count crew cursorTarget) > 0 
	//&& speed cursorTarget < 2 
	//&& (cursorTarget isKindOf "Car" 
	//|| cursorTarget isKindOf "Air" 
	//|| cursorTarget isKindOf "Ship")
	//']];

		//Gathers Diamonds
		life_actions = life_actions + [player addAction["<t color='#AAF200'>Recolter du diamant</t>",life_fnc_gatherDiamond,"",1000,false,false,"",'
		!life_action_inUse && ((player distance (getMarkerPos "diamond_1") < 150)) && (vehicle player == player) && (life_carryWeight + (["diamond"] call life_fnc_itemWeight)) <= life_maxWeight ']];

		//Gathers Salt 
		life_actions = life_actions + [player addAction["<t color='#AAF200'>Recolter du sel</t>",life_fnc_gatherSalt,"",1000,false,false,"",'
		!life_action_inUse && ((player distance (getMarkerPos "salt_1") < 150)) && (vehicle player == player) && (life_carryWeight + (["salt"] call life_fnc_itemWeight)) <= life_maxWeight ']];

		//Gathers Copper
		life_actions = life_actions + [player addAction["<t color='#AAF200'>Recolter du cuivre</t>",life_fnc_gatherCopper,"",1000,false,false,"",'
		!life_action_inUse && ((player distance (getMarkerPos "lead_1") < 150) OR (player distance (getMarkerPos "lead_2") < 150)) && (vehicle player == player) && (life_carryWeight + (["copperore"] call life_fnc_itemWeight)) <= life_maxWeight ']];

		//Gathers Iron
		life_actions = life_actions + [player addAction["<t color='#AAF200'>Recolter du fer</t>",life_fnc_gatherIron,"",1000,false,false,"",'
		!life_action_inUse && ((player distance (getMarkerPos "iron_1") < 150)) && (vehicle player == player) && (life_carryWeight + (["ironore"] call life_fnc_itemWeight)) <= life_maxWeight ']];

		//Gathers Sand 
		life_actions = life_actions + [player addAction["<t color='#AAF200'>Ramasser du sable</t>",life_fnc_gatherSand,"",1000,false,false,"",'
		!life_action_inUse && ((player distance (getMarkerPos "sand_1") < 150)) && (vehicle player == player) && (life_carryWeight + (["sand"] call life_fnc_itemWeight)) <= life_maxWeight ']];

		//Gathers Rock
		life_actions = life_actions + [player addAction["<t color='#AAF200'>Casser des cailloux</t>",life_fnc_gatherRock,"",1000,false,false,"",'
		!life_action_inUse && ((player distance (getMarkerPos "rock_1") < 150)) && (vehicle player == player) && (life_inv_pickaxe > 0) && (life_carryWeight + (["rock"] call life_fnc_itemWeight)) <= life_maxWeight ']];

		//Gather Oil
		life_actions = life_actions + [player addAction["<t color='#AAF200'>Recolter du pétrole</t>",life_fnc_gatherOil,"",1000,false,false,"",'
		!life_action_inUse && ((player distance (getMarkerPos "oil_1") < 150) OR (player distance (getMarkerPos "oil_2") < 150) OR (player distance (getMarkerPos "oil_3") < 150)) && (vehicle player == player) && (life_inv_pickaxe > 0) && (life_carryWeight + (["oil"] call life_fnc_itemWeight)) <= life_maxWeight ']];
	   		
		//Gather Phosphore
		life_actions = life_actions + [player addAction["<t color='#AAF200'>Recolter du phosphore</t>",life_fnc_gatherPhos,"",1000,false,false,"",'
		!life_action_inUse && ((player distance (getMarkerPos "meth_1") < 150)) && (vehicle player == player) && (life_inv_pickaxe > 0) && (life_carryWeight + (["phos"] call life_fnc_itemWeight)) <= life_maxWeight ']];
	   
		//Depanneur : Mettre en fourriere
		life_actions = life_actions + [player addAction["<t color='#00ffff'>Mettre en fourrière</t>",life_fnc_impoundAction,"",99,false,false,"",' 
		(typeOf (vehicle player) == "C_Offroad_01_F")
        && vehicle player != player		
		&& ((vehicle player animationPhase "HideServices") == 0) 
		&& ((vehicle player) in life_vehicles) 
		&& license_civ_dep 
		&& (speed vehicle player) < 1 
		&& !isNull cursorTarget
		&& ((cursorTarget isKindOf "Car") OR (cursorTarget isKindOf "Air") OR (cursorTarget isKindOf "Ship")) 	
		']];
		//Depanneur : Reparer
		life_actions = life_actions + [player addAction["<t color='#00ffff'>Dépanner le vehicule</t>",life_fnc_serviceTruck,"",99,false,false,"",' 
		(typeOf (vehicle player) == "C_Offroad_01_F") 
		&& ((vehicle player animationPhase "HideServices") == 0) 
		&& ((vehicle player) in life_vehicles) 
		&& license_civ_dep 
		&& (speed vehicle player) < 1
		&& !isNull cursorTarget
		&& ((cursorTarget isKindOf "Car") OR (cursorTarget isKindOf "Air") OR (cursorTarget isKindOf "Ship"))
		&& (damage cursorTarget) > 0.01 
		']];
		
		//Custom Heal
		life_actions = life_actions + [player addAction["<t color='#AAF200'>Me soigner (100%)</t>",life_fnc_heal,"",99,false,false,"",' vehicle player == player && (damage player) > 0.25 && ("FirstAidKit" in (items player))']];

		life_actions = life_actions + [player addAction["<t color='#AAF200'>Soigner cette personne (100%)</t>",life_fnc_heal_person,cursorTarget,9999999,false,false,"",'
		vehicle player == player
		&& !isNull cursorTarget 
		&& cursorTarget isKindOf "Man"
		&& (isPlayer cursorTarget)
		&& player distance cursorTarget < 3.5 
		&& ("FirstAidKit" in (items player))
		']];
	};
};

/*
	Undecided actions
life_actions = life_actions + [player addAction["Repair Vehicle ($500)",life_fnc_pumpRepair,"",999,false,false,"",
' vehicle player != player && (typeOf cursorTarget == "Land_fs_feed_F") && (vehicle player) distance cursorTarget < 6 ']];
life_actions = life_actions + [player addAction["Place Spike Strip",{if(!isNull life_spikestrip) then {detach life_spikeStrip; life_spikeStrip = ObjNull;};},"",999,false,false,"",'!isNull life_spikestrip']];
//Use Chemlights in hand
life_actions = life_actions + [player addAction["Chemlight (RED) in Hand",life_fnc_chemlightUse,"red",-1,false,false,"",
' isNil "life_chemlight" && "Chemlight_red" in (magazines player) && vehicle player == player ']];
life_actions = life_actions + [player addAction["Chemlight (YELLOW) in Hand",life_fnc_chemlightUse,"yellow",-1,false,false,"",
' isNil "life_chemlight" && "Chemlight_yellow" in (magazines player) && vehicle player == player ']];
life_actions = life_actions + [player addAction["Chemlight (GREEN) in Hand",life_fnc_chemlightUse,"green",-1,false,false,"",
' isNil "life_chemlight" && "Chemlight_green" in (magazines player) && vehicle player == player ']];
life_actions = life_actions + [player addAction["Chemlight (BLUE) in Hand",life_fnc_chemlightUse,"blue",-1,false,false,"",
' isNil "life_chemlight" && "Chemlight_blue" in (magazines player) && vehicle player == player ']];
//Drop Chemlight
life_actions = life_actions + [player addAction["Drop Chemlight",{if(isNil "life_chemlight") exitWith {};if(isNull life_chemlight) exitWith {};detach life_chemlight; life_chemlight = nil;},"",-1,false,false,"",'!isNil "life_chemlight" && !isNull life_chemlight && vehicle player == player ']];
//Custom Heal
life_actions = life_actions + [player addAction["<t color='#FF0000'>Heal Self</t>",life_fnc_heal,"",99,false,false,"",' vehicle player == player && (damage player) > 0.25 && ("FirstAidKit" in (items player)) && (currentWeapon player == "")']];
//Custom Repair
life_actions = life_actions + [player addAction["<t color='#FF0000'>Repair Vehicle</t>",life_fnc_repairTruck,"",99,false,false,"", ' vehicle player == player && !isNull cursorTarget && ((cursorTarget isKindOf "Car") OR (cursorTarget isKindOf "Air") OR (cursorTarget isKindOf "Ship")) && (damage cursorTarget) > 0.001 && ("ToolKit" in (backpackItems player)) && (player distance cursorTarget < ((boundingBox cursorTarget select 1) select 0) + 2) ']];
//Service Truck Stuff
life_actions = life_actions + [player addAction["<t color='#0000FF'>Service Nearest Car</t>",life_fnc_serviceTruck,"",99,false,false,"",' (typeOf (vehicle player) == "C_Offroad_01_F") && ((vehicle player animationPhase "HideServices") == 0) && ((vehicle player) in life_vehicles) && (speed vehicle player) < 1 ']];
life_actions = life_actions +
[player addAction["Push",life_fnc_pushVehicle,"",0,false,false,"",
'!isNull cursorTarget && player distance cursorTarget < 4.5 && cursorTarget isKindOf "Ship"']];
*/