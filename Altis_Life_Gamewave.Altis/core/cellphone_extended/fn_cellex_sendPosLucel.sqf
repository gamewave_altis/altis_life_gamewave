/*

	if send has been clicked

*/

private["_index", "_message","_asadmin","_msgtype","_dstdata","_dst"];
if((time - life_action_delay) < 2) exitWith {hint "Vous appuyez trop vite !"};
life_action_delay = time;
//_asadmin = _this select 0;
if (!("ItemGPS" in (assignedItems player))) exitWith {hint "Vous n'avez pas de GPS !"; closeDialog 0;};
_index = lbCurSel 1500;
//_message = ctrlText 1400;
if(_index == -1) exitWith {};
//if(_message == "") exitWith {};
//if(_asadmin && ((call life_adminlevel) < 1)) exitWith {};
_message = "Notification GPS";
ctrlSetText [1400, ""];


//Load data
_msgtype = lbValue[1500,_index];
_dstdata = lbData[1500,_index];

if(_msgtype != 0) exitWith {closeDialog 0; closeDialog 0; titleText["\n\n\n\n\n\n\n\n Vous devez selectionner un contact pour envoyer votre localisation", "PLAIN",0];};

//Send POS
switch(_msgtype) do 
{
	case 0: //To a person
	{
		_dst = call compile format["%1",_dstdata];
		if(isNull _dst) exitWith {closeDialog 0; titleText["\n\n\n\n\n\n\n\n Vous devez selectionner un contact pour envoyer votre localisation", "PLAIN",0];};
		if(isNil "_dst") exitWith {closeDialog 0; titleText["\n\n\n\n\n\n\n\n Vous devez selectionner un contact pour envoyer votre localisation", "PLAIN",0];};
		
		
		[[_message,name player,8,player],"TON_fnc_clientMessage",_dst,false] spawn life_fnc_MP;
		
	};
};

hint "Position envoyé.";