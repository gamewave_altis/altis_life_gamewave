#include <macro.h>
/*
	File: fn_p_openMenu.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Opens the players virtual inventory menu
*/
if(!alive player || dialog) exitWith {}; //Prevent them from opening this for exploits while dead
//if((player getVariable "restrained")) exitWith {hint "Vous �tes attach� !";closeDialog 0;closeDialog 0;};
createDialog "playerSettings";
disableSerialization;
switch(playerSide) do
{
	case west: 
	{
		ctrlShow[2011,false];
	};
	
	case civilian:
	{
		ctrlShow[2012,false];
	};
};

if(license_civ_merc) then
{
	ctrlShow[2012,true];
};

if(__GETC__(life_adminlevel) < 1) then
{
	ctrlShow[2020,false];
	ctrlShow[2021,false];
};

[] call life_fnc_p_updateMenu;

if(__GETC__(life_adminlevel) < 1) then
{
	ctrlShow[2020,false];
	ctrlShow[2021,false];
};

ctrlShow[1337,false];

if (player getVariable "c4") then {
	ctrlShow[1337,true];
};

//INDISPENSABLE ROLE PLAY BY LUCEL : ANIMATION D'INVENTAIRE ! VU A LA TV ! AMAZING !
if((player getVariable "restrained")||(player getVariable "Escorting")||(player getVariable "Surrender")) exitWith {};
if (vehicle player == player && isTouchingGround player && !(player getVariable ["restrained", false]) && !(player getVariable ["Escorting", false]) && !life_istazed && !life_knockout) then
{
	[] spawn {
		
		while{dialog}do{
		
		
		
		//Stand
		if (animationState player == "AmovPercMstpSnonWnonDnon") then 
		{
			player playMoveNow "AinvPercMstpSnonWnonDnon"; 
			sleep 1;
			player playMoveNow "AinvPercMstpSnonWnonDnon_G01"; 
			sleep 1;
			waitUntil{((animationState player) != "AinvPercMstpSnonWnonDnon_G01");};
		};
		//Stand + Rifle
		if (animationState player == "AmovPercMstpSrasWrflDnon") then 
		{
			player playMoveNow "AinvPercMstpSrasWrflDnon"; 
			sleep 1;	
			player playMoveNow "AinvPercMstpSrasWrflDnon_G01"; 
			sleep 1;
			waitUntil{((animationState player) != "AinvPercMstpSrasWrflDnon_G01");};
		};
		//Stand + Gun
		if (animationState player == "AmovPercMstpSrasWpstDnon") then 
		{
			player playMoveNow "AinvPercMstpSrasWpstDnon"; 
			sleep 1;	
			player playMoveNow "AinvPercMstpSrasWpstDnon_G01"; 
			sleep 1;	
			waitUntil{((animationState player) != "AinvPercMstpSrasWpstDnon_G01");};
		};	
		//Stand + Launcher
		if (animationState player == "amovpercmstpsraswlnrdnon") then 
		{
		
			player playMoveNow "AinvPercMstpSrasWlnrDnon"; 
			sleep 1;		
			player playMoveNow "AinvPercMstpSrasWlnrDnon_G01"; 
			sleep 1;
			waitUntil{((animationState player) != "AinvPercMstpSrasWlnrDnon_G01");};
		};	
		//Stand + Jumelle
		if (animationState player == "AmovPercMstpSoptWbinDnon") then 
		{
		
			player playMoveNow "AinvPercMstpSnonWnonDnon"; 
			sleep 1;		
			player playMoveNow "AinvPercMstpSnonWnonDnon_G01"; 
			sleep 1;
			waitUntil{((animationState player) != "AinvPercMstpSnonWnonDnon_G01");};
		};
		
		
		
		//Crouch
		if (animationState player == "AmovPknlMstpSnonWnonDnon") then 
		{
			player playMoveNow "AinvPknlMstpSnonWnonDnon";
			sleep 1;	
			player playMoveNow "AinvPknlMstpSnonWnonDnon_G01";
			sleep 1;	
			waitUntil{((animationState player) != "AinvPknlMstpSnonWnonDnon_G01");};
		};	
		//Crouch + Rifle
		if (animationState player == "AmovPknlMstpSrasWrflDnon") then 
		{
			player playMoveNow "AinvPknlMstpSrasWrflDnon";
			sleep 1;		
			player playMoveNow "AinvPknlMstpSrasWrflDnon_G01";
			sleep 1;	
			waitUntil{((animationState player) != "AinvPknlMstpSrasWrflDnon_G01");};
		};	
		//Crouch + Gun
		if (animationState player == "AmovPknlMstpSrasWpstDnon") then 
		{
			player playMoveNow "AinvPknlMstpSrasWpstDnon";
			sleep 1;	
			player playMoveNow "AinvPknlMstpSrasWpstDnon_G01";
			sleep 1;	
			waitUntil{((animationState player) != "AinvPknlMstpSrasWpstDnon_G01");};
		};	
		//Crouch + Launcher
		if (animationState player == "AmovPknlMstpSrasWlnrDnon") then 
		{
			player playMoveNow "AinvPknlMstpSrasWlnrDnon";
			sleep 1;	
			player playMoveNow "AinvPknlMstpSrasWlnrDnon_G01";
			sleep 1;	
			waitUntil{((animationState player) != "AinvPknlMstpSrasWlnrDnon_G01");};
		};	

		//Prone
		if (animationState player == "AmovPpneMstpSnonWnonDnon") then 
		{
			player playMoveNow "AinvPpneMstpSnonWnonDnon";
			sleep 1;	
			player playMoveNow "AinvPpneMstpSnonWnonDnon_G01";
			sleep 1;	
			waitUntil{((animationState player) != "AinvPpneMstpSnonWnonDnon_G01");};
		};	
		//Prone + Rifle
		if (animationState player == "AmovPpneMstpSrasWrflDnon") then 
		{
			player playMoveNow "AinvPpneMstpSrasWrflDnon";
			sleep 1;		
			player playMoveNow "AinvPpneMstpSrasWrflDnon_G01";
			sleep 1;	
			waitUntil{((animationState player) != "AinvPpneMstpSrasWrflDnon_G01");};
		};	
		//Prone + Gun
		if (animationState player == "AmovPpneMstpSrasWpstDnon") then 
		{
			player playMoveNow "AinvPpneMstpSrasWpstDnon";
			sleep 1;	
			player playMoveNow "AinvPpneMstpSrasWpstDnon_G01";
			sleep 1;	
			waitUntil{((animationState player) != "AinvPpneMstpSrasWpstDnon_G01");};
		};		

		
	};
		
		
		player playActionNow "stop";
		
	};
};