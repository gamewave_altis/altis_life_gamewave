/*
	File: fn_escortAction.sqf
*/

private["_unit"];


if(side player == west) then {
_unit = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
if(isNil "_unit" OR isNull _unit OR !isPlayer _unit OR side _unit != civilian) exitWith {};
//Je suis menott� ?
if(player getVariable "restrained") exitWith {};
//La cible est d�ja escort� ?
if(_unit getVariable "Escorting") exitWith {};
if((player distance _unit > 3)) exitWith {};
_unit attachTo [player,[0.1,1.1,0]];
_unit setVariable["transporting",false,true];
_unit setVariable["Escorting",true,true];
player reveal _unit;
}
else
{
if(side player == civilian) then {
_unit = cursorTarget;
if(isNil "_unit" OR isNull _unit OR !isPlayer _unit) exitWith {};
if((player distance _unit > 3)) exitWith {};
//Je suis menott� ?
if(player getVariable "restrained") exitWith {};
//La cible est menott� par un policier ?
if(_unit getVariable "CopRestrain") exitWith {};
//La cible est d�ja escort� ?
if(_unit getVariable "Escorting") exitWith {};

_unit attachTo [player,[0.1,1.1,0]];
_unit setVariable["transporting",false,true];
_unit setVariable["Escorting",true,true];
//player setVariable["currentlyEscorting",true,true];
player reveal _unit;
}
};
