/*
Scan tracker/C4
altislife.fr
POIL
*/

private["_vehicle","_nearVehicles","_trackerList","_pnjPos","_upp","_ui","_progress","_pgText","_cP","_check","_action"];
_pnjPos = _this select 0;
_price = 3500;
if(vehicle player != player) then
{
	_vehicle = vehicle player;
}
	else
{
	_nearVehicles = nearestObjects[getPos (_this select 0),["Car","Air","Ship"],20]; //Fetch vehicles within 30m.
	if(count _nearVehicles > 0) then
	{
		{
			if(!isNil "_vehicle") exitWith {}; //Kill the loop.
			_vehicle = _x;
			
		} foreach _nearVehicles;
	};
};
if(isNil "_vehicle") exitWith {hint "Il n'y a pas de véhicules à proximité"};
if(isNull _vehicle) exitWith {};

if(life_flouze < _price) exitWith {hint format["Pas assez d'argent."]};
life_flouze = life_flouze - _price;

if((count crew _vehicle) > 0) exitWith {[[4,"<t size='3'><t color='#FF0000'>Impossible de débuter le scanner</t></t> <br/><t size='1.5'>Pour des raisons de sécurité veuillez faire décendre tout les passagers du véhicule.</t>"],"life_fnc_broadcast",player,false] spawn life_fnc_MP};	
_trackerList = _vehicle getVariable "vehicle_info_c4";

if ((count _nearVehicles > 1)) exitWith {hint "Attend il y'a plusieurs véhicules dans la zone, j'arrive pas à me concentrer, éloigne en quelques uns !"};

	life_action_inUse = true;
	_upp = "Analyse du véhicule..";
	[[player, "scanner_lucel",10],"life_fnc_playSound",true,false] spawn Life_fnc_MP;
	//Setup our progress bar.
	disableSerialization;
	5 cutRsc ["life_progress","PLAIN"];
	_ui = uiNameSpace getVariable "life_progress";
	_progress = _ui displayCtrl 38201;
	_pgText = _ui displayCtrl 38202;
	_pgText ctrlSetText format["%2 (1%1)...","%",_upp];
	_progress progressSetPosition 0.01;
	_cP = 0.02;
	_check = true;
	while{true} do
	{
		sleep 0.25;
		_cP = _cP + 0.01;
		_progress progressSetPosition _cP;
		_pgText ctrlSetText format["%3 (%1%2)...",round(_cP * 100),"%",_upp];
		if(_cP >= 1) exitWith {};
		if(player distance _vehicle > 10) exitWith {};
		if(!alive player) exitWith {};
		if((count crew _vehicle) > 0) exitWith {_check = false};
	};
	5 cutText ["","PLAIN"];
	if(!alive player) exitWith {life_action_inUse = false;};
	
	if(!_check) exitWith {
	[[4,"<t size='3'><t color='#FF0000'>Interruption du scanner.</t></t> <br/><t size='1.5'>Vous ne devez pas interagir avec le véhicule pendant le scanner.</t>"],"life_fnc_broadcast",player,false] spawn life_fnc_MP;
	life_action_inUse = false;
	};	
		
		if((count _trackerList > 0) && _check) then 
			{
					[[player, "trackerfound_lucel",10],"life_fnc_playSound",true,false] spawn Life_fnc_MP;
					_action = [
								format["Une bombe type C4 vient d'être détectée sur votre véhicule, voulez-vous la désamorcer ?"],
								"Résultat du scanner :",
								"Oui",
								"Non"
							] call BIS_fnc_guiMessage;

					if(_action) then {
					_vehicle setVariable["vehicle_info_c4",[],true];
					[[4,"<t size='3'><t color='#00FF00'>C4 désamorcé.</t></t> <br/><t size='1.5'>Votre véhicule n'est plus piégé.</t>"],"life_fnc_broadcast",player,false] spawn life_fnc_MP;
					} else {
					[[4,"<t size='3'><t color='#FF0000'>C4 détecté.</t></t> <br/><t size='1.5'>Un C4 est détécté et toujours actif sur votre véhicule, soyez prudent.</t>"],"life_fnc_broadcast",player,false] spawn life_fnc_MP;
					};		
			}
			else
			{
					[[4,"<t size='3'><t color='#00FF00'>Aucun résultat.</t></t><br/><t size='1.5'>Aucune anomalie détectée sur votre véhicule.</t>"],"life_fnc_broadcast",player,false] spawn life_fnc_MP;
			};
			life_action_inUse = false;		
			
			
			

 



