/*
	File: fn_processAction.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Master handling for processing an item.
*/
if((time - life_action_delay) < 2.5) exitWith {hint "Vous appuyez trop vite !"};
life_action_delay = time;
private["_vendor","_type","_itemInfo","_oldItem","_oldItem2","_newItem","_cost","_upp","_hasLicense","_itemName","_oldVal","_oldVal2","_ui","_progress","_pgText","_cP","_error"];
_vendor = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
_type = [_this,3,"",[""]] call BIS_fnc_param;
//Error check
if(isNull _vendor OR _type == "" OR (player distance _vendor > 10)) exitWith {};
if(vehicle player != player) exitWith{hint "Euuh tu crois que je vais décharger à ta place ?"};
//unprocessed item,processed item, cost if no license,Text to display (I.e Processing  (percent) ..."
_error = false; // used below check the comment there

_itemInfo = switch (_type) do
{
	case "iron": {["ironore","iron_r",720,"Traitement de Fer",false]};
	case "sand": {["sand","glass",600,"Traitement de Sable",false]};
	case "houblon": {["houblon","biere",900,"Brassage",false];};
	case "cement": {["rock","cement",720,"Melange du Ciment",false]};
	case "salt": {["salt","salt_r",750,"Traitement de Sel",false]};
	case "copper": {["copperore","copper_r",1680,"Traitement de Cuivre",false]};
	case "oil": {["oilu","oilp",1680,"Traitement du Petrole",false];};
	case "diamond": {["diamond","diamondc",1800,"Traitement de Diamants",false]};
	case "marijuana": {["cannabis","marijuana",2550,"Traitement de Cannabis",false]};
	case "heroin": {["heroinu","heroinp",2700,"Traitement Heroine",false]};
	case "cocaine": {["cocaine","cocainep",2925,"Traitement de Cocaine",false]};
	case "meth": {["phos","meth",3000,"Préparation Méthamphétamine",true,"soude"]};
	default {[]};
};

//Error checking
if(count _itemInfo == 0) exitWith {};

//Setup vars.
_2var = _itemInfo select 4;  // true if process action is with 2 Items and false if processing with 1 Item.
_oldItem = _itemInfo select 0;
_newItem = _itemInfo select 1;
_cost = _itemInfo select 2;
_upp = _itemInfo select 3;
if(_2var) then { _oldItem2 = _itemInfo select 5; }; //set Itemname if (processing with 2 Items = true) 

if(_vendor in [mari_processor,coke_processor,heroin_processor]) then {
	_hasLicense = true;
} else {
	_hasLicense = missionNamespace getVariable (([_type,0] call life_fnc_licenseType) select 0);
};

_itemName = [([_newItem,0] call life_fnc_varHandle)] call life_fnc_varToStr;
_oldVal = missionNamespace getVariable ([_oldItem,0] call life_fnc_varHandle);

//2vars
if(_2var) then { _oldVal2 = missionNamespace getVariable ([_oldItem2,0] call life_fnc_varHandle); }; //calculate the amount of the second Item (for example Iron)
 
if(_2var) then { 
       if(_oldVal !=_oldVal2) then {
          _error = true; // True if amount of Item1 =! amount of Item 2 to prevent processing 20 FuelF with 20x oilp  and 1x iron_r)
       };
};
if(_error) exitWith{hint "Vous devez avoir un poid équitable pour vos deux produits."};

_cost = _cost * _oldVal;
//Some more checks
if(_oldVal == 0) exitWith {};

//Setup our progress bar.
disableSerialization;
5 cutRsc ["life_progress","PLAIN"];
_ui = uiNameSpace getVariable "life_progress";
_progress = _ui displayCtrl 38201;
_pgText = _ui displayCtrl 38202;
_pgText ctrlSetText format["%2 (1%1)...","%",_upp];
_progress progressSetPosition 0.01;
_cP = 0.01;

life_is_processing = true;

if(_hasLicense) then
{
	while{true} do
	{
		sleep  0.3;
		_cP = _cP + 0.01;
		_progress progressSetPosition _cP;
		_pgText ctrlSetText format["%3 (%1%2)...",round(_cP * 100),"%",_upp];
		if(_cP >= 1) exitWith {};
		if(player distance _vendor > 10) exitWith {};
	};
	
	if(player distance _vendor > 10) exitWith {hint "Tu dois être à moins de 10m"; 5 cutText ["","PLAIN"]; life_is_processing = false;};
	//2vars
	if(_2var) then 
	{
	([false,_oldItem2,_oldVal2] call life_fnc_handleInv); //delete the second items (for example Iron)
	};
	
	if(!([false,_oldItem,_oldVal] call life_fnc_handleInv)) exitWith {5 cutText ["","PLAIN"]; life_is_processing = false;};
	if(!([true,_newItem,_oldVal] call life_fnc_handleInv)) exitWith {5 cutText ["","PLAIN"]; [true,_oldItem,_oldVal] call life_fnc_handleInv; life_is_processing = false;};
	5 cutText ["","PLAIN"];
	titleText[format["Vous avez traité %1 en %2",_oldVal,_itemName],"PLAIN"];
	life_is_processing = false;
}
	else
{
	if(life_flouze < _cost) exitWith {hint format["Tu as besoin de %1 € pour traiter sans license !",[_cost] call life_fnc_numberText]; 5 cutText ["","PLAIN"]; life_is_processing = false;};
	//2vars
	if(_2var) then 
	{
	([false,_oldItem2,_oldVal2] call life_fnc_handleInv); //delete the second items (for example Iron)
	};
		
	while{true} do
	{
		sleep  0.9;
		_cP = _cP + 0.01;
		_progress progressSetPosition _cP;
		_pgText ctrlSetText format["%3 (%1%2)...",round(_cP * 100),"%",_upp];
		if(_cP >= 1) exitWith {};
		if(player distance _vendor > 10) exitWith {};
	};
	
	if(player distance _vendor > 10) exitWith {hint "Tu dois rester à moins de 10 mètres pour traiter !"; 5 cutText ["","PLAIN"]; life_is_processing = false;};
	if(life_flouze < _cost) exitWith {hint format["Tu as besoin de %1€ pour traiter sans license !",[_cost] call life_fnc_numberText]; 5 cutText ["","PLAIN"]; life_is_processing = false;};
	if(!([false,_oldItem,_oldVal] call life_fnc_handleInv)) exitWith {5 cutText ["","PLAIN"]; life_is_processing = false;};
	if(!([true,_newItem,_oldVal] call life_fnc_handleInv)) exitWith {5 cutText ["","PLAIN"]; [true,_oldItem,_oldVal] call life_fnc_handleInv; life_is_processing = false;};
	5 cutText ["","PLAIN"];
	titleText[format["Vous avez tranformer %1 en %2 pour %3€",_oldVal,_itemName,[_cost] call life_fnc_numberText],"PLAIN"];
	life_flouze = life_flouze - _cost;
	life_is_processing = false;
};	