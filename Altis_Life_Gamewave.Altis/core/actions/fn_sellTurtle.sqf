/*
	File: fn_sellTurtle.sqf
	
	Description:
	Sells the turtles! Save the turtles!
	This was a super lazy thing to do but I just want to push it...
*/
if(life_inv_turtle == 0) exitWith {
	titleText["Tu n'as pas de tortue à vendre !","PLAIN"];
};

_price = round(life_inv_turtle * 4500);
if([false,"turtle",life_inv_turtle] call life_fnc_handleInv) then {
	life_flouze = life_flouze + _price;
	titleText[format["Tu as vendu %1 tortue(s) pour %2€",round(_price / 4500),[_price] call life_fnc_numberText],"PLAIN"];
};