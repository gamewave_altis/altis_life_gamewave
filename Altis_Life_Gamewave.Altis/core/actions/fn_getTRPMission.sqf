/*

	File: fn_getTRPMission.sqf
	Author: For GameWave
	Donne une mission de livraison Pegasus avec control de véhicule
	
*/


private["_nearVehicles","_trp","_lieu","_target","_vehicle","_color"];

	_nearVehicles = nearestObjects[getPos (_this select 0),["Car","Air","Ship"],20]; //Recherche de vehicules dans les 20 m
	
	if(count _nearVehicles > 0) then
	{
		{
			if(!isNil "_vehicle") exitWith {}; //Evite les boucles.
			_vehData = _x getVariable["vehicle_info_owners",[]];

			if(count _vehData  > 0) then
			{
				_vehOwner = (_vehData select 0) select 0;
				if((getPlayerUID player) == _vehOwner) exitWith
				{
					_vehicle = _x;
				};
			};
		} foreach _nearVehicles;
	};

if(isNil "_vehicle") exitWith {["MessagePNJ",["T'es venu à pied? Rapproches ton camion qu'on le charge..."]] call BIS_fnc_showNotification };
if(isNull _vehicle) exitWith {};

if (typeOf _vehicle != "I_Truck_02_box_F") then 
	{
		["MessagePNJ",["Tu vas pas livrer avec ca, prends le camion Pegasus"]] call BIS_fnc_showNotification 
	};
	
_veh = _vehicle;
//_color = [(typeOf _veh),(_veh getVariable "Life_VEH_color")] call life_fnc_vehicleColorStr;
_color = [typeOf _veh] call life_fnc_vehicleColorCfg;
diag_log format ["PEGASUS CHECK || Veh: %1 COLOR :%2", _veh, _color];

/*
if (typeOf _vehicle == "I_Truck_02_box_F") then 
	{
		["MessagePNJ",["Viens avec le Camion Pegasus..."]] call BIS_fnc_showNotification; 
	};	
*/

if (typeOf _vehicle == "I_Truck_02_box_F") then 
{
	_target = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;

		if(str(_target) in life_trp_points) then
		{
			private["_point"];
			_point = life_trp_points - [(str(_target))];
			_trp = _point call BIS_fnc_selectRandom;
		}
		else
		{
			_trp = life_trp_points call BIS_fnc_selectRandom;
		};

	life_trp_start = _target;
	life_delivery_in_progress = true;
	life_trp_point = call compile format["%1",_trp];

	_trp = [_trp,"_"," "] call KRON_Replace;
	_lieu = _trp;
/*
Pegasus1 = Kavala
Pegasus2 = Athira
Pegasus3 = Sofia
Pegasus4 = Panagia
Pegasus5 = Zaros			
Pegasus6 = Agios Dionysios
Pegasus7 = Abdera (DP5)
*/
	
		switch (_lieu) do
			{
				case "Pegasus 1":
					{
						_lieu = "de Kavala (GPS: 048.128)";
					};

				case "Pegasus 2":
					{
						_lieu = "d'Athira (GPS: 145.188)";
					};

				case "Pegasus 3":
					{
						_lieu = "de Sofia (GPS: 252.226)";
					};

				case "Pegasus 4":
					{
						_lieu = "de Panagia (GPS: 202.088)";
					};

				case "Pegasus 5":
					{
						_lieu = "de Zaros (GPS: 092.121)";
					};

				case "Pegasus 6":
					{
						_lieu = "d'Agios Dionysios(GPS:102.159)";
					};

				case "Pegasus 7":
					{
						_lieu = "d'Abder (GPS: 093.211)";
					};
			};
	
		life_cur_task = player createSimpleTask [format["Livraison_%1",life_trp_point]];
		//[[name player, position life_trp_points],"life_fnc_createMarkerPegasus",civ,false] spawn BIS_fnc_MP;
		life_cur_task setSimpleTaskDescription [format["Livre les colis au dépôt %1",_lieu],"Mission de Livraison Pegasus",""];
		life_cur_task setTaskState "Assigned";
		player setCurrentTask life_cur_task;
		["DeliveryAssigned",[format["Livre ça au dépôt %1",_lieu]]] call BIS_fnc_showNotification;
		
};

	[] spawn
	{
		waitUntil {!life_delivery_in_progress OR !alive player};
		if(!alive player) then
			{
				life_cur_task setTaskState "Failed";
				player removeSimpleTask life_cur_task;
				["DeliveryFailed",["Tu es mort... Tu as perdu ta Mission Pegasus."]] call BIS_fnc_showNotification;
				life_delivery_in_progress = false;
				life_trp_point = nil;
			};
	};
