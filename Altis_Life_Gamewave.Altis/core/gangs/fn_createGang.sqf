#include <macro.h>
/*
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Pulls up the menu and creates the gang?
*/
private["_gangName","_length","_badChar","_chrByte","_allowed"];
disableSerialization;

_gangName = ctrlText (getControl(2520,2522));
_length = count (toArray(_gangName));
_chrByte = toArray (_gangName);
_allowed = toArray("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_ ");
if(_length > 32) exitWith {hint "Pas plus de 32 caractères."};
_badChar = false;
{if(!(_x in _allowed)) exitWith {_badChar = true;};} foreach _chrByte;
if(_badChar) exitWith {hint "Caractères invalide. Ne peut contenir que des Nombres et des Lettres avec underscore";};
if(life_dabflouze < (__GETC__(life_gangPrice))) exitWith {hint format["Pas assez d'argent pour créer un gang !.\n\nIl vous manque: %1€",[((__GETC__(life_gangPrice))-life_dabflouze)] call life_fnc_numberText];};

[[player,getPlayerUID player,_gangName],"TON_fnc_insertGang",false,false] spawn life_fnc_MP;
hint "Envoi des informations au serveur.....";
closeDialog 0;
life_action_gangInUse = true;
closeDialog 0;