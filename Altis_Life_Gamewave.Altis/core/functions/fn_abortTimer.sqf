/*
	Based on Havena abortEnabled.sqf // altisrp.fr // based on John "Paratus" VanderZwet Script 
	Modified by lecul
*/

private["_t","_l"];
disableSerialization;

while {true} do
{
waitUntil{!isNull(findDisplay 49)}; //Wait for ESC menu to be opened
_t = 20;

				while {(_t > 0) && (!isNull(findDisplay 49))} do
				{
				((findDisplay 49) displayCtrl 1010) ctrlEnable false;
				((findDisplay 49) displayCtrl 104) ctrlEnable false;
				((findDisplay 49) displayCtrl 1010) ctrlSetText format["Synchronisation - Patientez %1 sec",_t];
				((findDisplay 49) displayCtrl 104) ctrlSetText format["Synchronisation - Patientez %1 sec",_t];
					_l = 10;
					while {(_l > 1) && (!isNull(findDisplay 49))} do
					{
						sleep 0.1;
						_l = _l -1 ;
					};
					_t = _t -1 ;
					if(_t == 10) then {
						[] call life_fnc_sessionUpdate;
					};
				};	

				((findDisplay 49) displayCtrl 1010) ctrlSetText format["Suicide"];
				((findDisplay 49) displayCtrl 104) ctrlSetText format["Quitter"];
				((findDisplay 49) displayCtrl 1010) ctrlEnable true;
				((findDisplay 49) displayCtrl 104) ctrlEnable true;
				waitUntil{isNull(findDisplay 49)};	
};

