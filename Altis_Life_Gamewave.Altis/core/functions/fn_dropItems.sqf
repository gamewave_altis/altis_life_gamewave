/*
	File: fn_dropItems.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Called on death, player drops any 'virtual' items they may be carrying.
	LUCEL WAS HERE ! 
	trop malin ce lucel !
*/
private["_obj","_unit","_item","_value","_varList","_valueList","_handled"];
_unit = _this select 0;
_varList = [];
_valueList = [];
_handled = false;
{
	_item = _x;
	_value = missionNamespace getVariable _item;
	if(_value > 0) then
			{
				_var = [_item,1] call life_fnc_varHandle;
				_varList pushBack _var;
				_valueList pushBack _value;	
				missionNamespace setVariable[_x,0];	
				_handled = true;
			};
	
} foreach (life_inv_items);
//si objet true alors on cr�e la malette , mais une seule borel de merde !
if(_handled) then
{
				_pos = _unit modelToWorld[0,3,0];
				_pos = [_pos select 0, _pos select 1, 0];
				_obj = "Land_Suitcase_F" createVehicle _pos;
				[[_obj],"life_fnc_simDisable",nil,true] spawn life_fnc_MP;
				_obj setPos _pos;
				//_obj setVariable["item",[_var,_value],true];
				_obj setVariable["item",[_varList,_valueList],true];
				//stockage du nom + value dans Items
				diag_log format["item %1",_obj getVariable "item"];
				//missionNamespace setVariable[_x,0];
				diag_log format["_varList %1",_varList];
				//diag_log format["_valueList %1",_valueList];

};

{
_item = _x;
_value = missionNamespace getVariable _item;
switch(_item) do
	{	

		case "life_flouze":
		{
			if(life_flouze > 0) then
			{
				_pos = _unit modelToWorld[0,3,0];
				_pos = [_pos select 0, _pos select 1, 0];
				_obj = "Land_Money_F" createVehicle _pos;
				_obj setVariable["item",["money",_value],true];
				_obj setPos _pos;
				[[_obj],"life_fnc_simDisable",nil,true] spawn life_fnc_MP;
				missionNamespace setVariable[_x,0];
			};
		};
	};
	
} foreach (["life_flouze"]);