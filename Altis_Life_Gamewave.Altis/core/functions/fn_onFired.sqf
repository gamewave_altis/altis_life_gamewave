/*
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Handles various different ammo types being fired.
	
    unit: Object - Object the event handler is assigned to
    weapon: String - Fired weapon
    muzzle: String - Muzzle that was used
    mode: String - Current mode of the fired weapon
    ammo: String - Ammo used
    magazine: String - magazine name which was used
    projectile: Object - Object of the projectile that was shot (Arma 2: OA and onwards)

	
*/
private["_ammoType","_projectile","_weapon","_player"];
_player = _this select 0;
_weap = currentWeapon _player;
_ammoType = _this select 4; 
_projectile = _this select 6;

//if(_weap == "hgun_P07_snds_F") then {
//	_ammo = _player ammo _weap;
//		if (_ammo > 0) then {
//			_player setAmmo ["hgun_P07_snds_F",0];
//		};
//	};


if(_ammoType == "GrenadeHand_stone") then {


	_projectile spawn {
		private["_position"];
		while {!isNull _this} do {
			_position = getPosATL _this;
			sleep 0.1;
		};
		[[_position],"life_fnc_flashbang",true,false] spawn life_fnc_MP;
	};
};
