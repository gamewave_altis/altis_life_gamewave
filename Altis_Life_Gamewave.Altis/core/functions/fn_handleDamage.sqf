/*
	File: fn_handleDamage.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Handles damage, specifically for handling the 'tazer' pistol and nothing else.
*/
private["_unit","_damage","_source","_projectile","_part","_curWep","_isUnconscious","_namekiller","_namekilled","_type","_uidkilled","_uidkiller"];
_unit = _this select 0;
_part = _this select 1;
_damage = _this select 2;
_source = _this select 3;
_projectile = _this select 4;
_isUnconscious = _unit getVariable "FAR_isUnconscious";
_namekiller = name _source;
_namekilled =  name _unit;
_type = "";
_uidkiller = getPlayerUID _source;
_uidkilled = getPlayerUID _unit;
_handled = true;

//Handle the tazer first (Top-Priority).
if(!isNull _source) then {
	if(_source != _unit) then {
		_curWep = currentWeapon _source;
		if(_projectile in ["B_45ACP_Ball"] && _curWep in ["hgun_Pistol_heavy_02_Yorris_F"]) then {
				private["_distance","_isVehicle","_isQuad"];
				_distance = if(_projectile == "B_45ACP_Ball") then {100} else {35};
				_isVehicle = if(vehicle player != player) then {true} else {false};
				_isQuad = if(_isVehicle) then {if(typeOf (vehicle player) == "B_Quadbike_01_F") then {true} else {false}} else {false};
				
				_damage = 0;
				_handled = false;
				if(_unit distance _source < _distance) then {
					if(!life_istazed && !(_unit getVariable["restrained",false])) then {
						if(_isVehicle && _isQuad) then {
							player action ["Eject",vehicle player];
							[_unit,_source] spawn life_fnc_tazed;
						} else {
							[_unit,_source] spawn life_fnc_tazed;
						};
					};
				};
			//Temp fix for super tasers on cops.
			//if(playerSide == west && side _source == west) then {
			_damage = 0;
			};
		};
	};
// Flashbang
if (_projectile in ["mini_Grenade"]) then {
	_damage = 0;
	[_projectile] spawn life_fnc_handleFlashbang;
};
/*		
if (alive _unit && _damage >= 1 && _part !="") then 
	{			
		_unit setVariable["LetalDamageLucel",FALSE,TRUE];
	};
*/
if (_handled && alive _unit && _damage >= 0.9 && _isUnconscious == 0 && _part !="") then 
	{	
	

		diag_log format ["FAR condition Unit alive: %1, damage:%2, isUnconscious: %3, isHit: %4", alive _unit, _damage, _isUnconscious, _part];
		_handled = false;
		//_unit setDamage 0;
		_unit allowDamage false;
		_damage = 0;
		_type = "Inconscience";
		[_unit, _source] spawn FAR_Player_Unconscious;
	
	if(side _source != west && alive _source && _source != _unit) then
	{
		if(vehicle _source isKindOf "LandVehicle") then
		{
			if(alive _source) then
			{
				[[getPlayerUID _source,name _source,"187V"],"life_fnc_wantedAdd",false,false] spawn life_fnc_MP;
				_type = "Inconscience Carkill";
			};
		}
		else
		{
			[[getPlayerUID _source,name _source,"187"],"life_fnc_wantedAdd",false,false] spawn life_fnc_MP;
			_type = "Inconscience par balle";
		};
	[[_namekilled,_namekiller,_type,_uidkiller,_uidkilled],"DB_fnc_logDeath",false,false] spawn life_fnc_MP;
	};
};	

[] call life_fnc_hudUpdate;
_damage;