/*
	File: fn_hudSetup.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Setups the hud for the player?
*/
private["_display","_alpha","_version","_p","_pg"];
disableSerialization;
if(life_hudDisable)exitWith{};
_display = findDisplay 46;
_alpha = _display displayCtrl 1001;
_version = _display displayCtrl 1000;
_damage = life_damage;
_realDamage = (-(_damage/100) +1);
diag_log format ["_realDamage: %1",_realDamage];
2 cutRsc ["playerHUD","PLAIN"];
_version ctrlSetText format["BETA: 0.%1.%2",(productVersion select 2),(productVersion select 3)];
player setDamage _realDamage;
diag_log format ["damageplayer: %1", damage player];
[] call life_fnc_hudUpdate;

[] spawn
{
	private["_dam"];
	while {true} do
	{
		_dam = damage player;
		life_damage = (-(_dam -1)*100);
		waitUntil {(damage player) != _dam};
		[] call life_fnc_hudUpdate;
	};
};