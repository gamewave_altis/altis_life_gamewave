/*
	File: fn_updateCash.sqf
	Author: Skalicon
	
	Description:
	Updates the players Inventory and ATM cash.
*/
private ["_type","_modifier","_amount"];
_type = _this select 0;
_modifier = _this select 1;
_amount = _this select 2;

if (_type == "atm") then {
	if (_modifier == "add") then {
		life_dabflouze = life_dabflouze + _amount;
		//[[1, player, format["ATM: Added %1 and now has %2",_amount,life_dabflouze]],"ASY_fnc_logIt",false,false] spawn life_fnc_MP;
	};
	if (_modifier == "take") then {
		life_dabflouze = life_dabflouze - _amount;
		//[[1, player, format["ATM: Removed %1 and has %2 remaining",_amount,life_dabflouze]],"ASY_fnc_logIt",false,false] spawn life_fnc_MP;
	};
	if (_modifier == "set") then {
		life_dabflouze = _amount;
		//[[1, player, format["ATM: Set to the amount of %1",life_dabflouze]],"ASY_fnc_logIt",false,false] spawn life_fnc_MP;
	};
	life_dabflouzeCache = (life_dabflouze / 2) + 3;
};

if (_type == "cash") then {
	if (_modifier == "add") then {
		life_flouze = life_flouze + _amount;
		//[[1, player, format["Cash: Added %1 and now has %2",_amount,life_flouze]],"ASY_fnc_logIt",false,false] spawn life_fnc_MP;
	};
	if (_modifier == "take") then {
		life_flouze = life_flouze - _amount;
		//[[1, player, format["Cash: Removed %1 and has %2 remaining",_amount,life_flouze]],"ASY_fnc_logIt",false,false] spawn life_fnc_MP;
	};
	if (_modifier == "set") then {
		life_flouze = _amount;
		//[[1, player, format["Cash: Set to the amount of %1",life_flouze]],"ASY_fnc_logIt",false,false] spawn life_fnc_MP;
	};
	life_flouzeCache = (life_flouze / 2) + 5;
};




