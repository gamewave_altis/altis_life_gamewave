/*
	File: fn_ticketAction.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Starts the ticketing process.
*/
private["_unit"];
_unit = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
disableSerialization;
if(!(createDialog "life_med_ticket_give")) exitWith {hint "Impossible d'ouvrir l'interface"};
if(isNull _unit OR !isPlayer _unit) exitwith {};
ctrlSetText[2651,format["Honoraire : %1",name _unit]];
life_med_ticket_unit = _unit;