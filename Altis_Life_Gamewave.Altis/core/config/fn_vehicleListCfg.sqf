#include <macro.h>
/*
	File: fn_vehicleListCfg.sqf
	Modif Nonoxs for GameWave
	
	Description:
	Vendeurs Véhicules
*/
private["_shop","_return"];
_shop = [_this,0,"",[""]] call BIS_fnc_param;
if(_shop == "") exitWith {[]};
_return = [];
switch (_shop) do
{
/*
****************************
***********CIVILS***********
****************************	
*/
// ********************
// ******* Kart *******
// ********************
	case "kart_shop":
	{
		if(__GETC__(life_donator) == 0) then
		{
			_return =
			[
			["C_Kart_01_Blu_F",15000],
			["C_Kart_01_Fuel_F",15000],
			["C_Kart_01_Red_F",15000],
			["C_Kart_01_Vrana_F",15000]
			];
		};			
		
		if(__GETC__(life_donator) > 1) then
		{
			_return =
			[
			["C_Kart_01_Blu_F",10500],
			["C_Kart_01_Fuel_F",10500],
			["C_Kart_01_Red_F",10500],
			["C_Kart_01_Vrana_F",10500]
			];
		};					
	};	
// ********************
// ******* CARS *******
// ********************
	case "civ_car":
	{
		if(__GETC__(life_donator) == 0) then	
		{
			_return =
			[
				["B_Quadbike_01_F",5000],
				["C_Hatchback_01_F",15000],
				["C_Offroad_01_F",25000],
				["C_SUV_01_F",35000],
				["C_Van_01_transport_F",90000]
			];

		};	
				
		if(__GETC__(life_donator) > 1) then	
		{
			_return =
			[
				["B_Quadbike_01_F",3500],
				["C_Hatchback_01_F",10500],
				["C_Hatchback_01_sport_F",42500],
				["C_Offroad_01_F",17500],
				["C_SUV_01_F",24500],
				["C_Van_01_transport_F",63000]
			];

		};				
	};	
	
// ********************
// ********************	
// ****** Trunck ******
// ********************
	case "civ_truck":
	{
		if(__GETC__(life_donator) == 0) then
		{
			_return =
			[
				["C_Van_01_box_F",110000],
				["I_Truck_02_transport_F",200000],
				["I_Truck_02_covered_F",250000],
				["B_Truck_01_transport_F",530000],
				["B_Truck_01_box_F",780000],
				["O_Truck_03_device_F",780000],
				["I_Truck_02_box_F",50000] //Camion Pegasus
			];	
		};

		if(__GETC__(life_donator) >1) then
		{
			_return =
			[
				["C_Van_01_box_F",77000],
				["C_Van_01_Fuel_F",50000],
				["I_Truck_02_transport_F",140000],
				["I_Truck_02_covered_F",175000],
				["B_Truck_01_transport_F",371000],
				["B_Truck_01_box_F",546000],
				["O_Truck_03_device_F",546000],
				["I_Truck_02_box_F",50000] //Camion Pegasus
			];	
		};
	};

// ********************
	
	case "civ_Pegasus_1":
	{
		_return =
		[
			["I_Truck_02_box_F",50000] //Camion Pegasus
		];	
	};
// ********************
// *******  AIR *******
// ********************	
	case "civ_air":
	{
		if(__GETC__(life_donator) == 0) then
		{
			_return =
			[
				["B_Heli_Light_01_F",750000],
				["C_Heli_Light_01_civil_F",600000],
				["O_Heli_Light_02_unarmed_F",1000000]
			];	

		};

		if(__GETC__(life_donator) >1) then
		{
			_return =
			[
				["B_Heli_Light_01_F",525000],
				["C_Heli_Light_01_civil_F",600000],
				["O_Heli_Light_02_unarmed_F",700000],
				["I_Heli_Transport_02_F",1275000], //mohak
				["O_Heli_Transport_04_F",1275000] // taru
		
			];
		};

	};


// ********************
// *******  MER *******
// ********************	
	case "civ_ship":
	{
		if(__GETC__(life_donator) == 0) then
		{
			_return =
			[
			["C_Rubberboat",5000],
			["C_Boat_Civil_01_F",22000],
			["I_G_Boat_Transport_01_F",15000]
			];
		};			
	
		if(__GETC__(life_donator) > 1) then
		{
			_return =
			[
			["C_Rubberboat",3500],
			["C_Boat_Civil_01_F",15400],
			["I_G_Boat_Transport_01_F",10500],
			["B_SDV_01_F",42500]
			];
		};					
	};


/*
****************************
**********REBELLES**********
****************************	
*/			
// ******* REBELLE *******

	case "reb_v":
	{
		if(__GETC__(life_donator) == 0) then
		{
			_return =
			[
				["B_Quadbike_01_F",5000],
				["C_SUV_01_F",35003],
				["O_MRAP_02_F",350000],
				["B_G_Offroad_01_F",35000],
				["B_G_Offroad_01_armed_F",750000],
				["B_Heli_Light_01_F",750000],
				["O_Heli_Light_02_unarmed_F",1000000],
				["I_Heli_light_03_unarmed_F",1500000],
				["O_Heli_Transport_04_bench_F",1500000],
				["O_Heli_Transport_04_covered_F",1500000]
			];

		};	
		
		
		if(__GETC__(life_donator) > 1) then
		{
			_return =
			[
				["B_Quadbike_01_F",3500],
				["C_SUV_01_F",24503],
				["O_MRAP_02_F",245000],
				["B_G_Offroad_01_F",24500],
				["B_G_Offroad_01_armed_F",525000],
				["B_Heli_Light_01_F",525000],
				["O_Heli_Light_02_unarmed_F",700000],
				["I_Heli_light_03_unarmed_F",1050000],
				["I_Heli_Transport_02_F",1275000],
				["O_Heli_Transport_04_F",1275000],
				["O_Heli_Transport_04_bench_F",1050000],
				["O_Heli_Transport_04_covered_F",1050000]
			];
					
		};		
	};	
	

/*
****************************
***********POLICE***********
****************************	
*/	
// ******* CARS *******
	
	case "cop_car":
	{
		_return =
		[
			["B_Quadbike_01_F",5000],
			["C_Offroad_01_F",30000],
			["C_SUV_01_F",30000]
		];
			if(__GETC__(life_coplevel) > 1) then //lvl 2 - Brigadier/Sergent -

			{	
				_return pushBack["B_MRAP_01_F",150000];
			};
				
				if(__GETC__(life_coplevel) > 2) then //lvl 3 - Adjudant/Adjudant-chef -
				{
					_return pushBack["B_G_Offroad_01_armed_F",250000];	
					_return pushBack["C_Hatchback_01_sport_F",50000];
				};

					
					if(__GETC__(life_coplevel) > 3) then //lvl 4 - Major/Aspirant -
					{
						_return pushBack["I_MRAP_03_F",150000];
						
					};

		if(__GETC__(life_donator) > 1) then
		{
		_return =
		[
			["B_Quadbike_01_F",3500],
			["C_Offroad_01_F",17500],
			["C_SUV_01_F",24500]
		];
			if(__GETC__(life_coplevel) > 1) then //lvl 2 - Brigadier/Sergent -

			{	
				_return pushBack["B_MRAP_01_F",100000];
			};
				
				if(__GETC__(life_coplevel) > 2) then //lvl 3 - Adjudant/Adjudant-chef -
				{
					_return pushBack["B_G_Offroad_01_armed_F",200000];	
					_return pushBack["C_Hatchback_01_sport_F",42500];
				};

					
					if(__GETC__(life_coplevel) > 3) then //lvl 4 - Major/Aspirant -
					{
						_return pushBack["I_MRAP_03_F",100000];
						
					};
		};
		
	};
	

	case "cop_air":
	{
			if(__GETC__(life_coplevel) > 1) then //lvl 2 - Brigadier/Sergent 
			{
				_return pushBack["B_Heli_Light_01_F",500000];	
				_return pushBack["O_Heli_Light_02_unarmed_F",750000];	
			};
				
				if(__GETC__(life_coplevel) > 2) then //lvl 3
				{
					_return pushBack["I_Heli_light_03_unarmed_F",1000000];	
				};
						
			if(__GETC__(life_coplevel) > 4) then //lvl 5 - Lieutenant
			{
				_return pushBack["B_Heli_Transport_01_F",1500000];	
				_return pushBack["B_Heli_Transport_03_unarmed_F",1800000];
				_return pushBack["O_Heli_Transport_04_bench_F",1800000];
			};

				if(__GETC__(life_coplevel) > 6) then //lvl 7 - Commandant -
				{
					_return pushBack["B_Heli_Attack_01_F",2000000];	
				};					
	};
	
	case "cop_ship":
	{
		_return =
		[
			["B_Boat_Transport_01_F",2000]
		];

			if(__GETC__(life_coplevel) > 1) then //lvl 2 + Hors-Bord Police
			{
				_return pushBack["C_Boat_Civil_01_police_F",30000];	
			};
			
				if(__GETC__(life_coplevel) > 3) then //lvl 4 + Sous-Marin+ Bateau Armé
				{
					_return pushBack["B_SDV_01_F",50000];
					_return pushBack["B_Boat_Armed_01_minigun_F",50000];	
				};

	};
	
/*	
**************************
**********Donator*********
**************************
*/	
	case "donator":
	{	
		if(__GETC__(life_donator) > 1) then
		{
			_return =
			[
				["B_Quadbike_01_F",3500],
				["C_Hatchback_01_F",10500],
				["C_Hatchback_01_sport_F",42500],
				["C_Offroad_01_F",17500],
				["C_SUV_01_F",24500],
				["C_Van_01_transport_F",63000]
			];
					if(license_civ_truck) then
					{
						_return pushBack ["C_Van_01_box_F",77000];
						_return pushBack ["C_Van_01_Fuel_F",50000];
						_return pushBack ["I_Truck_02_transport_F",140000];
						_return pushBack ["I_Truck_02_covered_F",175000];
						_return pushBack ["B_Truck_01_transport_F",371000];
						_return pushBack ["B_Truck_01_box_F",546000];
					};
					
						if(license_civ_rebel) then
						{
						_return pushBack ["B_G_Offroad_01_F",24500];
						_return pushBack ["B_G_Offroad_01_armed_F",525000];
						_return pushBack ["O_MRAP_02_F",245000];
						};
						
					if(license_civ_air) then
					{
						_return pushBack ["B_Heli_Light_01_F",525000]; // littlebird
						_return pushBack ["O_Heli_Light_02_unarmed_F",700000]; // orca
						_return pushBack ["I_Heli_Transport_02_F",1275000]; // mohak
						_return pushBack ["O_Heli_Transport_04_F",1275000]; // Taru
							
							if(license_civ_rebel) then
							{
							_return pushBack ["I_Heli_light_03_unarmed_F",1050000]; //hellcat
							};						
					};
		};					

	};

	
// *******
// *******  DEPANNEUR CARS
// *******

case "dep_shop":
	{
		if(__GETC__(life_donator) == 0) then	
		{
			_return =
			[
			["C_Offroad_01_F",35000]
			];
		};	

		if(__GETC__(life_donator) > 1) then	
		{
			_return =
			[
			["C_Offroad_01_F",24500]
			];
		};				
	};
	
case "dep_air":
	{
		if(__GETC__(life_donator) == 0) then	
		{
			_return =
			[
			["O_Heli_Transport_04_repair_F",1500000]
			];
		};	

		if(__GETC__(life_donator) > 1) then	
		{
			_return =
			[
			["O_Heli_Transport_04_repair_F",1050000]
			];
		};				
	};
	
// *******
// *******  TAXI CARS
// *******

case "taxi_shop":
	{
		if(__GETC__(life_donator) == 0) then	
		{
			_return =
			[
			["C_SUV_01_F",35000],
			["C_Offroad_01_F",25000] 
			];
		};	

		if(__GETC__(life_donator) > 1) then	
		{
			_return =
			[
			["C_SUV_01_F",24500],
			["C_Offroad_01_F",17500] 
			];
		};				
	};
	
// *******
// *******  MEDIC CARS
// *******

case "med_shop":
	{
		if(__GETC__(life_donator) == 0) then	
		{
			_return =
			[
			["C_SUV_01_F",35000],
			["C_Hatchback_01_F",15000]
			];
		};	

		if(__GETC__(life_donator) > 1) then	
		{
			_return =
			[
			["C_SUV_01_F",24500],
			["C_Hatchback_01_F",10500],
			["C_Hatchback_01_sport_F",42500]
			];
		};				
	};	
	
// *******
// *******  MEDIC AIR
// *******
	case "med_air_hs":
	{
		if(__GETC__(life_donator) == 0) then
		{
		_return =
			[
			["B_Heli_Light_01_F",750001],
			["O_Heli_Light_02_unarmed_F",1000001],
			["O_Heli_Transport_04_medevac_F",1500000]
			];
		};

		
		if(__GETC__(life_donator) > 1) then
		{
			_return =
			[
				["B_Heli_Light_01_F",525001],
				["O_Heli_Light_02_unarmed_F",700001],				
				["O_Heli_Transport_04_medevac_F",1050000]				
			];
		};

	};
	
// *******
// *******  MERCENAIRE AIR
// *******
case "merc_air":
	{
		if(__GETC__(life_donator) == 0) then
		{
			_return =
			[
				["B_Heli_Light_01_F",750005], //hm9
				["O_Heli_Light_02_unarmed_F",1000000], //orca
				["I_Heli_light_03_unarmed_F",1500000], //hellcat
				["O_Heli_Transport_04_bench_F",1500000],
				["O_Heli_Transport_04_covered_F",1500000]
			];	
		};
		
		if(__GETC__(life_donator) >1) then
		{
			_return =
			[
				["B_Heli_Light_01_F",525005],
				["O_Heli_Light_02_unarmed_F",700001], //orca
				["I_Heli_light_03_unarmed_F",1050000], //hellcat
				["O_Heli_Transport_04_bench_F",1050000],
				["O_Heli_Transport_04_covered_F",1050000]
			];

		};

	};
	
	
// *******
// *******  MERCENAIRE CARS
// *******
case "merc_v":
	{
		if(__GETC__(life_donator) == 0) then
		{
			_return =
			[
				["B_Quadbike_01_F",5000],
				["B_MRAP_01_F",350000],
				["I_MRAP_03_F",350000],
				["B_G_Offroad_01_F",35000],
				["B_G_Offroad_01_armed_F",750000],
				["C_SUV_01_F",35000]

			];

		};	
				
		if(__GETC__(life_donator) > 1) then
		{
			_return =
			[
				["B_Quadbike_01_F",3500],
				["B_MRAP_01_F",245000],
				["I_MRAP_03_F",245000],
				["B_G_Offroad_01_F",24500],
				["B_G_Offroad_01_armed_F",525000],
				["C_Hatchback_01_sport_F",42500],
				["C_SUV_01_F",24500]
			];
					
		};		
	};
	
// *******
// *******  MERCENAIRE SHIP
// *******
case "merc_ship":
	{
		if(__GETC__(life_donator) == 0) then
		{
			_return =
			[
			["C_Rubberboat",5000],
			["C_Boat_Civil_01_F",22000],
			["I_G_Boat_Transport_01_F",15000]
			];
		};			
		if(__GETC__(life_donator) == 1) then
		{
			_return =
			[
			["C_Rubberboat",4250],
			["C_Boat_Civil_01_F",18700],
			["I_G_Boat_Transport_01_F",12750],
			["B_SDV_01_F",50000]
			];
		};			
		if(__GETC__(life_donator) > 1) then
		{
			_return =
			[
			["C_Rubberboat",3500],
			["C_Boat_Civil_01_F",15400],
			["I_G_Boat_Transport_01_F",10500],
			["B_SDV_01_F",42500]
			];
		};					
	};		
	
};
			//["I_Truck_02_medical_F",25000],
			//["O_Truck_03_medical_F",45000],
			//["B_Truck_01_medical_F",60000],
			

_return;


