/*
	File: fn_itemWeight.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Gets the items weight and returns it.
*/
private["_item"];
_item = [_this,0,"",[""]] call BIS_fnc_param;
if(_item == "") exitWith {};

switch (_item) do
{
	case "apple": {1};
	case "rabbit": {2};
	case "salema": {2};
	case "ornate": {2};
	case "mackerel": {2};
	case "tuna": {6};
	case "mullet": {3};	
	case "catshark": {6};
	case "donuts": {1};
	case "pizza": {3};
	case "glace": {3};	
	
	
	case "water": {2};
	case "coffee": {2};		
	
	case "fuelE": {2};
	case "fuelF": {5};	
	
	case "ironore": {5};	
	case "iron_r": {3};
	case "sand": {3};
	case "glass": {2};	
	case "houblon": {4};
	case "biere": {2};	
	case "rock": {3};
	case "cement": {2};		
	case "salt": {3};
	case "salt_r": {2};	
	case "copperore": {5};	
	case "copper_r": {3};	
	case "oilu": {5};
	case "oilp": {4};
	case "diamond": {5};
	case "diamondc": {3};	

	case "cannabis": {4};
	case "marijuana": {3};
	case "heroinu": {4};
	case "heroinp": {3};
	case "cocaine": {4};
	case "cocainep": {3};

	case "turtle": {4};	
	case "turtlesoup": {2};	

	case "fishing": {2};
	case "money": {0};
	case "pickaxe": {2};
	case "spikeStrip": {5};
	case "menotte": {2};

	case "goldbar": {12};

	case "storagesmall": {5};
	case "storagebig": {10};
	
	case "meth": {2};
	case "phos": {2};
	case "soude": {2};
	
	case "mauer": {2};
	case "trackerlucel": {2};

	default {1};
};
