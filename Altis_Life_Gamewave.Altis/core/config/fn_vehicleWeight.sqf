/*

	File: fn_VehicleWeight.sqf

	Modif for GameWave

	

	Description:

	Base configuration for vehicle weight

*/
private["_vehicle","_weight","_used"];
_vehicle = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
if(isNull _vehicle) exitWith {};


_weight = -1;
_used = (_vehicle getVariable "Trunk") select 1;

switch ((typeOf _vehicle)) do

{

	//HELIDLC
		//REB
	case "O_Heli_Transport_04_bench_F": {_weight = 40;}; 
	case "O_Heli_Transport_04_covered_F": {_weight = 40;}; 
		//COP
	case "B_Heli_Transport_03_unarmed_F": {_weight = 100;}; 
		//CIV
	case "O_Heli_Transport_04_F": {_weight = 40;}; 
	case "C_Heli_Light_01_civil_F": {_weight = 30;}; 
	case "O_Heli_Transport_04_medevac_F": {_weight = 100;}; 
	case "O_Heli_Transport_04_repair_F": {_weight = 100;}; 
	
	
	//KARTDLC
	case "C_Kart_01_Blu_F": {_weight = 5;}; // Kart
	case "C_Kart_01_Fuel_F": {_weight = 5;}; // Kart
	case "C_Kart_01_Red_F": {_weight = 5;}; // Kart
	case "C_Kart_01_Vrana_F": {_weight = 5;}; // Kart


	case "B_Quadbike_01_F": {_weight = 30;}; // Quad

	case "C_Hatchback_01_F": {_weight = 70;}; // Voiture Hayon

	case "C_Hatchback_01_sport_F": {_weight = 70;}; // Voiture Hayon Sport	

	case "C_Offroad_01_F": {_weight = 95;}; //Pickup

	case "C_SUV_01_F": {_weight = 85;}; // SUV 

	

	case "C_Van_01_transport_F": {_weight = 200;}; // Camionnette

	case "C_Van_01_box_F": {_weight = 250;}; // Camionnette couvert 

	case "C_Van_01_Fuel_F": {_weight = 10;}; // Camion Citerne

	

	case "I_Truck_02_transport_F": {_weight = 285;}; // Zamak 

	case "I_Truck_02_covered_F": {_weight = 340;}; // Zamak Couvert	

	case "I_Truck_02_box_F": {_weight = 10;}; // Camion Pegasus 

	

	case "B_Truck_01_transport_F": {_weight = 575;}; // HEMTT 

	case "B_Truck_01_box_F": {_weight = 750;}; // HEMTT Couvert

	

	case "O_Truck_03_device_F": {_weight = 375;}; //Tempest

	

	case "B_G_Offroad_01_F": {_weight = 110;}; //Pickup rebelle

	case "B_G_Offroad_01_armed_F": {_weight = 90;}; //Pickup Arm�

	case "O_MRAP_02_F": {_weight = 60;}; // Ifrit

	

	case "B_MRAP_01_F": {_weight = 65;}; // Hunter

	case "B_MRAP_01_hmg_F": {_weight = 65;}; // Hunter Arm�	

	case "I_MRAP_03_F": {_weight = 60;}; // Strider

	case "B_Heli_Light_01_F": {_weight = 30;}; //MH-9 Hummingbird

	case "B_Heli_Attack_01_F": {_weight = 40;}; //Blackfoot

	case "O_Heli_Light_02_unarmed_F": {_weight = 70;}; // Po-30 Orca

	case "I_Heli_Transport_02_F": {_weight = 125;}; // Mohawk

	case "I_Heli_light_03_unarmed_F": {_weight = 40;}; //Hellcat

    case "B_Heli_Transport_01_F": {_weight = 40;}; //Ghost Hawk


	case "C_Rubberboat": {_weight = 90;}; // Petit bateau

	case "I_G_Boat_Transport_01_F": {_weight = 150;}; //Canot d'Assaut

	case "C_Boat_Civil_01_F": {_weight = 200;}; // Bateau Civil


	case "B_Boat_Transport_01_F": {_weight = 90;}; //Petit Bateau

	case "C_Boat_Civil_01_police_F": {_weight = 200;}; // Bateau Police

	case "B_Boat_Armed_01_minigun_F": {_weight = 150;}; // Bateau arme

	case "B_SDV_01_F": {_weight = 30;}; // Sous-Marin

	case "Land_CargoBox_V1_F": {_weight = 5000;};
	
	case "Box_IND_Grenades_F": {_weight = 375;};
	
	case "B_supplyCrate_F": {_weight = 750;};

};
if(isNil "_used") then {_used = 0};

[_weight,_used];
