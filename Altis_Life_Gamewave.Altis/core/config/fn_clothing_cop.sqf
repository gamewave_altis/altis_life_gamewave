#include <macro.h>
/*
	File: fn_clothing_cop.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Master config file for Cop clothing store.
*/
private["_filter","_ret"];
_filter = [_this,0,0,[0]] call BIS_fnc_param;
//Classname, Custom Display name (use nil for Cfg->DisplayName, price

//Shop Title Name
ctrlSetText[3103,"Altis Police Department Shop"];

_ret = [];
switch (_filter) do
{
	//Uniforms
	case 0:
	{
		_ret pushBack["U_Rangemaster","Cop Uniform",25];
		_ret pushBack["U_B_Wetsuit",nil,5000];
		if(__GETC__(life_coplevel) > 1) then
		{
			_ret pushBack["U_B_CombatUniform_mcam_vest",nil,350];
			_ret pushBack["U_B_CombatUniform_mcam_tshirt",nil,350];
		};
		if(__GETC__(life_coplevel) > 2) then
		{
			_ret pushBack["U_B_GhillieSuit",nil,550];
			_ret pushBack["U_B_survival_uniform",nil,550];
		};
	};
	
	//Hats
	case 1:
	{
		if(__GETC__(life_coplevel) > 1) then
		{	
			_ret pushBack["H_Cap_police",nil,120];
			_ret pushBack["H_HelmetB_plain_mcamo",nil,75];
			_ret pushBack["H_Booniehat_mcamo",nil,120];
			_ret pushBack["H_Cap_police",nil,120];
		};
		
		if(__GETC__(life_coplevel) > 2) then
		{
			_ret pushBack["H_MilCap_mcamo",nil,100];
			_ret pushBack["H_PilotHelmetHeli_B",nil,100];
			_ret pushBack["H_HelmetB_plain_mcamo",nil,100];
			_ret pushBack["H_HelmetIA_net",nil,100];
			_ret pushBack["H_Beret_brn_SF",nil,100];
		};
		if(__GETC__(life_coplevel) > 5) then
		{
			_ret pushBack["H_Beret_Colonel",nil,100];

		};
	};
	
	//Glasses
	case 2:
	{
			_ret pushBack["G_Diving",nil,75];
			_ret pushBack["G_Shades_Black",nil,75];
			_ret pushBack["G_Shades_Blue",nil,75];
			_ret pushBack["G_Sport_Blackred",nil,75];
			_ret pushBack["G_Sport_Checkered",nil,75];
			_ret pushBack["G_Sport_Blackyellow",nil,75];
			_ret pushBack["G_Sport_BlackWhite",nil,75];
			_ret pushBack["G_Aviator",nil,75];
			_ret pushBack["G_Squares",nil,75];
			_ret pushBack["G_Lowprofile",nil,75];
			_ret pushBack["G_Combat",nil,75];

		if(__GETC__(life_coplevel) > 3) then
		{
		_ret pushBack["G_Balaclava_blk",nil,75];
		};
	};
	
	//Vest
	case 3:
	{	
		_ret pushBack["V_TacVest_blk_POLICE",nil,800];		
		_ret pushBack["V_Rangemaster_belt",nil,800];
		_ret pushBack["V_RebreatherB",nil,5000];
		if(__GETC__(life_coplevel) > 1) then
		{
			_ret pushBack["V_PlateCarrier2_rgr",nil,1500];
			_ret pushBack["V_PlateCarrier1_blk",nil,1500];
			_ret pushBack["V_Chestrig_blk",nil,1500];
			_ret pushBack["V_TacVestIR_blk",nil,1500];
			_ret pushBack["V_BandollierB_blk",nil,1500];
		};
	};
	
	//Backpacks
	case 4:
	{
		_ret pushBack["B_Kitbag_cbr",nil,800];
		_ret pushBack["B_FieldPack_cbr",nil,800];
		_ret pushBack["B_AssaultPack_cbr",nil,800];
		_ret pushBack["B_Bergen_sgg",nil,800];
		if(__GETC__(life_coplevel) > 2) then
		{
		_ret pushBack["B_Carryall_cbr",nil,800];
		_ret pushBack["B_Carryall_oucamo",nil,800];
		_ret pushBack["B_FieldPack_blk",nil,800];
		_ret pushBack["B_Bergen_blk",nil,800];
		_ret pushBack["B_OutdoorPack_blk",nil,800];
		};
	};
};

_ret;