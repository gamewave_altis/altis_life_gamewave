/*
	File: fn_virt_shops.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Config for virtual shops.
*/
private["_shop"];
_shop = _this select 0;

switch (_shop) do
{
	case "heroin": {["Dealer de drogues",["cocainep","heroinp","marijuana","meth"]]};
	case "market": {["Marche Altis",["water","rabbit","apple","redgull","tbacon","pickaxe","fuelF","peach","biere","storagesmall","storagebig"]]};
	case "wongs": {["Restaurant des Wongs",["turtlesoup","turtle"]]};
	case "coffee": {["Café du Patron",["coffee","donuts"]]};
	case "gang": {["Gang Market", ["water","rabbit","apple","redgull","tbacon","lockpick","pickaxe","fuelF","peach","blastingcharge","boltcutter","menotte"]]};
	case "oil": {["Trader de Petrole",["oilp","pickaxe","fuelF"]]};
	case "fishmarket": {["Marche au Poisson",["salema","ornate","mackerel","mullet","tuna","catshark"]]};
	case "glass": {["Oliver de Garglass",["glass"]]};
	case "iron": {["Vendeur de fer",["iron_r","copper_r"]]};
	case "diamond": {["Joaillier",["diamondc"]]};
	case "salt": {["Vendeur de sel",["salt_r"]]};
	case "cop": {["Boutique Police",["scannerlucel","trackerlucel","donuts","coffee","spikeStrip","water","rabbit","apple","redgull","fuelF","mauer","defusekit"]]};
	case "cement": {["Vendeur de ciment",["cement"]]};
	case "blackmarket": {["Marche noir",["trackerlucel","cement","salt_r","diamondc","iron_r","copper_r","glass","oilp","apple","peach","lockpick","water","rabbit","redgull","tbacon","pickaxe","fuelF","biere","blastingcharge","boltcutter","menotte"]]};
	case "rebmarket": {["Marche Rebelle",["menotte","water","rabbit","apple","redgull","tbacon","lockpick","pickaxe","fuelF","peach","biere","blastingcharge","boltcutter"]]};
	case "civilprotection": {["Protection Civil",["menotte","redgull"]]};
	case "gold": {["Achat d'or",["goldbar"]]};
	case "med_shop": {["Marché médecin",["water","rabbit","apple","redgull","tbacon","fuelF","peach"]]};
	case "merc_shop": {["Marché mercenaire",["trackerlucel","menotte","defusekit","redgull","water","biere","rabbit","apple","tbacon","fuelF"]]};
	case "dep_shop": {["Marché dépanneur",["scannerlucel","redgull","water","biere","rabbit","apple","tbacon","fuelF"]]};

};