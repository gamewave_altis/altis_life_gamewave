/*
	File: fn_eatFood.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Main handling system for eating food.
	*Needs to be revised and made more modular and more indept effects*
*/
private["_food","_val","_sum","_sub"];
_food = [_this,0,"",[""]] call BIS_fnc_param;
if(_food == "") exitWith {};

if([false,_food,1] call life_fnc_handleInv) then {
	switch (_food) do
	{
		case "apple": {_val = 10};
		case "peach": {_val = 10};
		
		case "salema": {_val = 30};
		case "ornate": {_val = 20};
		case "mackerel": {_val = 20};
		case "tuna": {_val = 60};
		case "mullet": {_val = 30};
		case "catshark": {_val = 60};
		case "turtle": {_val = 100};
		case "turtlesoup": {_val = 100};
		case "donuts": {_val = 30};
		case "tbacon": {_val = 50};
		
		case "pizza": {_val = 50};
		case "glace": {_val = 20};
		
		
		case "rabbit":{ _val = 20};
	};

	_sum = life_hunger + _val;
	if(_sum > 100) then {_sum = 100; player setFatigue 1; hint "Vous avez trop mangé ! Vous vous sentez maintenant fatigué.";};
	life_hunger = _sum;
	
	if (_food == "glace") then {
		_sub = life_thirst + _val;
		if(_sub > 100) then {_sub = 100; player setFatigue 1; hint "T'es hydraté pour un Marathon.";};
		life_thirst = _sub;	
	};	
	
	
};