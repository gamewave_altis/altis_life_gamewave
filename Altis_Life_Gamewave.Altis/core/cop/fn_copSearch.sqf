#include <macro.h>
/*
	File: fn_copSearch.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Returns information on the search.
	Edit by Lucel // altislife.fr
*/
life_action_inUse = false;
private["_civ","_invs_legal","_license","_robber","_guns","_gun","_invs_illegal","_action","_legal","_illegal","_inv_legal","_inv_illegal"];
_civ = [_this,0,Objnull,[Objnull]] call BIS_fnc_param;
_invs_legal = [_this,1,[],[[]]] call BIS_fnc_param;
_invs_illegal = [_this,3,[],[[]]] call BIS_fnc_param;
_robber = [_this,2,false,[false]] call BIS_fnc_param;
if(isNull _civ) exitWith {};

_legal = 0;
_illegal = 0;
_inv_legal = "";
_inv_illegal = "";
if(count _invs_legal > 0) then
{
	{
		_inv_legal = _inv_legal + format["%1 %2<br/>",_x select 1,[([_x select 0,0] call life_fnc_varHandle)] call life_fnc_varToStr];

	} foreach _invs_legal;
	
}
	else
{
	_inv_legal = "Aucun objet.";
};

if(count _invs_illegal > 0) then
{
	{
		_inv_illegal = _inv_illegal + format["%1 %2<br/>",_x select 1,[([_x select 0,0] call life_fnc_varHandle)] call life_fnc_varToStr];
		_index = [_x select 0,__GETC__(sell_array)] call TON_fnc_index;
		if(_index != -1) then
		{
			_illegal = _illegal + ((_x select 1) * ((__GETC__(sell_array) select _index) select 1));
		};
	} foreach _invs_illegal;
	
	
}
	else
{
	_inv_illegal = "Aucun objet illégal.";
};





if(!alive _civ || player distance _civ > 5) exitWith {hint format["Vous ne pouvez pas fouiller %1", name _civ]};
//hint format["%1",_this];
hint parseText format["<t color='#FF9900'><t size='2'>%1</t></t>
<br/>
<t color='#AAF200'><t size='1.5'><br/>Légal</t></t>
<br/>%2
<br/>
<t color='#FF0000'><t size='1.5'><br/>Illégal</t></t>
<br/>%4
<br/>
<t color='#FF0000'>%3</t>"
,name _civ,_inv_legal,if(_robber) then {"Braqueur de banque"} else {""},_inv_illegal];

if(count _invs_illegal > 0) then
{
	_action = [
				format["Votre contrôle est positif, l'individu a en sa possession des objets ou substances illégales, voulez-vous lui saisir ?"],
				"Résultat du contrôle de Police :",
				"Oui",
				"Non"
			] call BIS_fnc_guiMessage;


	if(_action) then {
		[[false,(_invs_illegal select 0) select 0,(_invs_illegal select 0) select 1],"life_fnc_handleInv",_civ,false] spawn life_fnc_MP;
		//[false,(_invs_illegal select 0),(_invs_illegal select 1)] call life_fnc_handleInv;
		
		systemChat format["Objets illégaux supprimés. Vous avez saisi les objets illégaux de l'individu."];
		hint parseText format["<t size='3'><t color='#00FF00'>Objets illégaux supprimés.</t></t> <br/><t size='1.5'>Vous avez saisi les objets illégaux de l'individu.</t>"];
		
		[[0,format["%1 c'est fait attraper pour %2€ de stupéfiant sur lui.",name _civ,[_illegal] call life_fnc_numberText]],"life_fnc_broadcast",west,false] spawn life_fnc_MP;
		
		if(_illegal > 6000) then
		{
			[[getPlayerUID _civ,name _civ,"482"],"life_fnc_wantedAdd",false,false] spawn life_fnc_MP;
		};
		[[getPlayerUID _civ,name _civ,"481"],"life_fnc_wantedAdd",false,false] spawn life_fnc_MP;
	
	} else {
		systemChat format["Objets illégaux détectés. Mais vous avez choisi de ne pas lui saisir sa marchandise."];
		hint parseText format["<t size='3'><t color='#FF0000'>Objets illégaux détectés.</t></t> <br/><t size='1.5'>Mais vous avez choisi de ne pas lui saisir sa marchandise.</t>"];
	};	
};
	



if(_robber) then
{
	[[0,format["%1 c'est fait attraper pour braquage de banque.",name _civ]],"life_fnc_broadcast",true,false] spawn life_fnc_MP;
};