/*
	File: fn_seizePlayerWeaponAction.sqf
	Author: Skalicon
	
	Description:
	Removes the players weapons client side
*/
//removeAllWeapons player;
//if (uniform player in ["U_Rangemaster","U_B_CombatUniform_mcam_worn"]) then {removeUniform player;};
//if (vest player in ["V_HarnessOGL_brn"]) then {removeVest player;};
//license_civ_gun = false;
//[] call life_fnc_civFetchGear;

player removeWeapon "srifle_LRR_LRPS_F";
player removeWeapon "arifle_TRG20_F";
player removeWeapon "arifle_Katiba_F";
player removeWeapon "srifle_DMR_01_F";
player removeWeapon "arifle_SDAR_F";
player removeWeapon "srifle_EBR_ACO_F";
player removeWeapon "hgun_Pistol_heavy_02_Yorris_F";
player removeWeapon "hgun_Rook40_F";
player removeWeapon "LMG_Mk200_F";
player removeWeapon "hgun_Pistol_heavy_02_F";
player removeWeapon "hgun_PDW2000_F";
player removeWeapon "hgun_ACPC2_F";
player removeWeapon "hgun_P07_snds_F";
player removeWeapon "arifle_Mk20_F";
player removeWeapon "arifle_MXC_F";
player removeWeapon "srifle_EBR_F";
player removeWeapon "arifle_MXM_F";
player removeWeapon "srifle_GM6_SOS_F";
player removeWeapon "LMG_Zafir_F";
player removeWeapon "launch_RPG32_F";
player removeWeapon "hgun_Rook40_F";
player removeWeapon "hgun_Pistol_heavy_02_F";
player removeWeapon "hgun_Pistol_heavy_01_F";
player removeWeapon "hgun_ACPC2_F";
player removeWeapon "hgun_PDW2000_F";
player removeWeapon "SMG_01_F";
player removeWeapon "arifle_Katiba_C_F";
player removeWeapon "arifle_Mk20C_F";
player removeWeapon "arifle_MXC_Black_F";
player removeWeapon "arifle_MX_Black_F";
player removeWeapon "arifle_MXM_Black_F";
player removeWeapon "arifle_MX_SW_Black_F";
player removeWeapon "srifle_EBR_F";
player removeMagazine  "16Rnd_9x21_Mag";
player removeMagazine  "9Rnd_45ACP_Mag";
player removeMagazine  "6Rnd_45ACP_Cylinder";
player removeMagazine  "5Rnd_127x108_Mag";
player removeMagazine  "20Rnd_762x51_Mag";
player removeMagazine  "30Rnd_65x39_caseless_mag";
player removeMagazine  "30Rnd_556x45_Stanag";
player removeMagazine  "7Rnd_408_Mag";
player removeMagazine  "200Rnd_65x39_cased_Box";
player removeMagazine  "30Rnd_9x21_Mag";
player removeMagazine  "30Rnd_65x39_caseless_mag";
player removeMagazine  "20Rnd_556x45_UW_mag";
player removeMagazine  "6Rnd_45ACP_Cylinder";
player removeMagazine  "20Rnd_556x45_UW_mag";
player removeMagazine  "30Rnd_556x45_Stanag";
player removeMagazine  "10Rnd_762x51_Mag";
player removeMagazine  "20Rnd_762x51_Mag";
player removeMagazine  "30Rnd_65x39_caseless_green";
player removeMagazine  "150Rnd_762x51_Box";
player removeMagazine  "7Rnd_408_Mag";

[] call SOCK_fnc_updateRequest;

//[] call life_fnc_civLoadGear;
titleText["Vos armes ont été saisies.","PLAIN"];