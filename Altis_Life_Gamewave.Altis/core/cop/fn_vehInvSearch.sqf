#include <macro.h>
/*
	File: fn_vehInvSearch.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Searches the vehicle for illegal items.
	Edit by Lucel // altislife.fr
*/
private["_vehicle","_vehicleInfo","_value","_inv_legal","_inv_illegal","_legal","_illegal"];
_vehicle = cursorTarget;
if(isNull _vehicle) exitWith {};
if(!((_vehicle isKindOf "Air") OR (_vehicle isKindOf "Ship") OR (_vehicle isKindOf "LandVehicle"))) exitWith {};
_vehicleInfo = _vehicle getVariable ["Trunk",[]];
if(count _vehicleInfo == 0) exitWith {hint "Ce véhicule est vide !"};

_legal = "";
_illegal = "";
_inv_legal = [];
_inv_illegal = [];
_value_illegal = 0;

	life_action_inUse = true;
	_upp = "Fouille...";
	[[player, "Fouille",10],"life_fnc_playSound",true,false] spawn life_fnc_MP;
	//Setup our progress bar.
	disableSerialization;
	5 cutRsc ["life_progress","PLAIN"];
	_ui = uiNameSpace getVariable "life_progress";
	_progress = _ui displayCtrl 38201;
	_pgText = _ui displayCtrl 38202;
	_pgText ctrlSetText format["%2 (1%1)...","%",_upp];
	_progress progressSetPosition 0.01;
	_cP = 0.01;
	while{true} do
	{
		if(animationState player != "AinvPercMstpSnonWnonDnon_Putdown_AmovPercMstpSnonWnonDnon") then {
		[[player,"AinvPercMstpSnonWnonDnon_Putdown_AmovPercMstpSnonWnonDnon"],"life_fnc_animSync",true,false] spawn life_fnc_MP;
		player playMoveNow "AinvPercMstpSnonWnonDnon_Putdown_AmovPercMstpSnonWnonDnon";
		};
		sleep 0.09;
		_cP = _cP + 0.05;
		_progress progressSetPosition _cP;
		_pgText ctrlSetText format["%3 (%1%2)...",round(_cP * 100),"%",_upp];
		if(_cP >= 1) exitWith {};
		if(player distance _vehicle > 5) exitWith {};
		if(!alive player) exitWith {};

	};
	player playActionNow "stop";
	5 cutText ["","PLAIN"];
	if(!alive player) exitWith {life_action_inUse = false;};
	
{
	_item = _x select 0;
	_val = _x select 1;
	_index_legal = [_item,life_legal_items] call TON_fnc_index;
	_index_illegal = [_item,life_illegal_items] call TON_fnc_index;
	if(_index_legal != -1) then
	{
		if(_val > 0) then
		{
			_inv_legal pushBack [_item,_val];
			//[false,_item,_val] call life_fnc_handleInv;
		};
	};
	if(_index_illegal != -1) then
	{
		if(_val > 0) then
		{
			_inv_illegal pushBack [_item,_val];
			_vIndex = [_item,__GETC__(sell_array)] call TON_fnc_index;
			if(_vIndex != -1) then
			{
			_value_illegal = _value_illegal + (_val * ((__GETC__(sell_array) select _vIndex) select 1));
			};
			//[false,_item,_val] call life_fnc_handleInv;
		};
	};
} foreach (_vehicleInfo select 0);

//diag_log format ["SearchVEH _inv_legal %1",_inv_legal];
//diag_log format ["SearchVEH _inv_illegal %1",_inv_illegal];
//diag_log format ["SearchVEH _vehicle %1",_vehicle];
//diag_log format ["SearchVEH _value_illegal %1",_value_illegal];
if(count _inv_legal > 0) then
{
	{
		_legal = _legal + format["%1 %2<br/>",_x select 1,[([_x select 0,0] call life_fnc_varHandle)] call life_fnc_varToStr];

	} foreach _inv_legal;
	
}
	else
{
	_legal = "Aucun objet.";
	systemChat format["Rien de légal dans ce vehicule"];
	
};

if(count _inv_illegal > 0) then
{
	{
		_illegal = _illegal + format["%1 %2<br/>",_x select 1,[([_x select 0,0] call life_fnc_varHandle)] call life_fnc_varToStr];

	} foreach _inv_illegal;
	
}
	else
{
	_illegal = "Aucun objet.";
	systemChat format["Rien d'illégal dans ce vehicule"];
};
life_action_inUse = false;
//diag_log format ["SearchVEH _illegal %1",_illegal];
//diag_log format ["SearchVEH _legal %1",_legal];

// DISPLAY

hint parseText format["<t color='#FF9900'><t size='2'>%1</t></t>
<br/>
<t color='#AAF200'><t size='1.5'><br/>Légal</t></t>
<br/>%2
<br/>
<t color='#FF0000'><t size='1.5'><br/>Illégal</t></t>
<br/>%3
<br/>
<br/>"
,getText(configFile >> "CfgVehicles" >> typeOf _vehicle >> "displayName"),_legal,_illegal];

// DIALOG SI ILLEGAL

if(count _inv_illegal > 0) then
{
	closedialog 0;
	_action = [
				format["Votre contrôle est positif, le véhicule contient des objets ou substances illégales, voulez-vous les saisirs ?"],
				"Résultat du contrôle de Police :",
				"Oui",
				"Non"
			] call BIS_fnc_guiMessage;


	if(_action) then {
	
	
	
		//Broadcast to all
		[[0,format["La police vient de trouver %1€ de drogues dans un véhicule !",[_value_illegal] call life_fnc_numberText]],"life_fnc_broadcast",true,false] spawn life_fnc_MP;
		
		systemChat format["Objets illégaux supprimés. Vous avez saisi les objets illégaux du véhicule"];
		
		hint parseText format["<t size='3'><t color='#00FF00'>Objets illégaux supprimés.</t></t> <br/><t size='1.5'>Vous avez saisi les objets illégaux de l'individu.</t>"];
		
		systemChat format["Félicitation, Vous avez gagné %1€ sur cette fouille",[_value_illegal] call life_fnc_numberText];
		life_dabflouze = life_dabflouze + _value_illegal;
		
		_vehicle setVariable["Trunk",[_inv_legal],true];
	
	} else {
		systemChat format["Objets illégaux détectés. Mais vous avez choisi de ne pas lui saisir sa marchandise."];
		hint parseText format["<t size='3'><t color='#FF0000'>Objets illégaux détectés.</t></t> <br/><t size='1.5'>Mais vous avez choisi de ne pas lui saisir sa marchandise.</t>"];
	};	
};
