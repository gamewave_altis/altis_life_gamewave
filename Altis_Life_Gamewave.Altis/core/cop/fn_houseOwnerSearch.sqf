#include <macro.h>
/*
*/
private["_house","_owner"];
_house = cursorTarget;
if(isNull _house) exitWith {hint "Pas de maison ??"};
if(!(_house isKindOf "House_F")) exitWith {};

_owner = _house getVariable ["life_homeOwnerName", []];

if(count _owner == 0) then {
	hint "Cette maison est encore a vendre.";
} else {
	hint format ["%1 est le propriétaire de cette maison.", (_owner select 0)];
};


