/*
Altislife.fr by lucel
*/
private["_licName","_licTxt"];
if((lbCurSel 2005) == -1) exitWith {hint "Vous n'avez selectionné aucun permis."};
_licName = lbData[2005,(lbCurSel 2005)];
_licTxt = [_licName] call life_fnc_varToStr;

if (_licName == "license_civ_rebel") exitWith {hint "Impossible de retirer ce permis"};
if (_licName == "license_civ_merc") exitWith {hint "Impossible de retirer ce permis, contactez un admin pour retirer la licence mercenaire"};
if (_licName == "license_civ_heroin") exitWith {hint "Impossible de retirer ce permis"};
if (_licName == "license_civ_marijuana") exitWith {hint "Impossible de retirer ce permis"};
if (_licName == "license_civ_houblon") exitWith {hint "Impossible de retirer ce permis"};
if (_licName == "license_civ_coke") exitWith {hint "Impossible de retirer ce permis"};
if (_licName == "license_civ_diamond") exitWith {hint "Impossible de retirer ce permis"};
if (_licName == "license_civ_copper") exitWith {hint "Impossible de retirer ce permis"};
if (_licName == "license_civ_iron") exitWith {hint "Impossible de retirer ce permis"};
if (_licName == "license_civ_sand") exitWith {hint "Impossible de retirer ce permis"};
if (_licName == "license_civ_salt") exitWith {hint "Impossible de retirer ce permis"};
if (_licName == "license_civ_cement") exitWith {hint "Impossible de retirer ce permis"};
if (_licName == "license_civ_home") exitWith {hint "Impossible de retirer ce permis"};


[[player,_licName],"life_fnc_licensesremoveCiv",life_pInact_curTarget,FALSE] spawn life_fnc_MP;

closeDialog 0;

hint parseText format["<t size='3'><t color='#FF0000'>Permis : </t></t> <br/><t size='1.5'>Vous venez de retirer le permis suivant : <t color='#00FF00'>%1</t></t>",_licTxt];

systemChat format["Vous avez retiré le permis suivant : %1",_licTxt];
